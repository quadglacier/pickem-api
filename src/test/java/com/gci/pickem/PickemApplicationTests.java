package com.gci.pickem;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore // until I can figure out the problem with 11 upgrade.
public class PickemApplicationTests {

	@Test
	public void contextLoads() {
	}

}
