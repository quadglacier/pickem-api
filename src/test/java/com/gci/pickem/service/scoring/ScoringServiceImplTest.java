package com.gci.pickem.service.scoring;

import com.gci.pickem.data.*;
import com.gci.pickem.model.ScoreResponse;
import com.gci.pickem.repository.*;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Ignore // until I can figure out the problem with 11 upgrade.
public class ScoringServiceImplTest {

    @Autowired private ScoringServiceImpl scoringService;

    @Autowired private UserRepository userRepository;
    @Autowired private PickRepository pickRepository;
    @Autowired private GameRepository gameRepository;
    @Autowired private PoolRepository poolRepository;
    @Autowired private TeamRepository teamRepository;

    private List<TeamEntity> teams = new ArrayList<>();

    private long userId;
    private long poolId;

    @Before
    public void setup() {
        createTeams();

        PoolEntity pool = new PoolEntity();
        pool.setPoolName("Test Pool 1");

        pool = poolRepository.save(pool);

        poolId = pool.getPoolId();

        UserEntity user = new UserEntity();
        user.setFirstName("Jordan");
        user.setLastName("Test");

        user = userRepository.save(user);

        userId = user.getUserId();

        UserPoolEntity userPool = new UserPoolEntity();
        userPool.setUserId(userId);
        userPool.setPoolId(poolId);

        user.setUserPools(Sets.newHashSet(userPool));

        createPicks(userId, poolId);
    }

    @Test
    public void test() {
        ScoreResponse score = scoringService.getScore(userId, poolId, 2017, 1);

        // 13, 14, 15, 16 are not complete yet. 136 is the max minus 58 = 78
        assertEquals(78, score.getUserScores().get(0).getWeekScore());
    }

    private GameEntity createGame(long homeTeamId, long awayTeamId, long winningTeamId, boolean complete) {
        GameEntity game = new GameEntity();

        game.setWeek(1);
        game.setSeason(2017);
        game.setHomeTeamId(homeTeamId);
        game.setAwayTeamId(awayTeamId);
        game.setWinningTeamId(winningTeamId);
        game.setGameComplete(complete);

        return gameRepository.save(game);
    }

    private void createTeams() {
        for (long i = 1; i < 33; i++) {
            TeamEntity team = new TeamEntity();
            team.setAbbreviation(Long.toString(i));
            team.setTeamName(String.format("Team %d", i));
            team.setCity(String.format("City %d", i));
            team.setExternalId(i);

            teams.add(teamRepository.save(team));
        }
    }

    private void createPicks(long userId, long poolId) {
        for (int i = 0; i < 16; i++) {
            GameEntity game =
                createGame(
                    teams.get(i).getTeamId(),
                    teams.get(i + 16).getTeamId(),
                    teams.get(i).getTeamId(),
                    i < 12);

            PickEntity pick = new PickEntity();
            pick.setUserId(userId);
            pick.setPoolId(poolId);
            pick.setGameId(game.getGameId());
            pick.setChosenTeamId(teams.get(i).getTeamId());
            pick.setConfidence(i + 1); // scores start at 1 and go to 16.

            pick = pickRepository.save(pick);

            pick.setGame(game);
        }
    }
}