package com.gci.pickem.service.mail;

import com.google.common.collect.Sets;
import com.sendwithus.SendWithUs;
import com.sendwithus.SendWithUsSendRequest;
import com.sendwithus.exception.SendWithUsException;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class SendWithUsMailServiceTest {

    private SendWithUsMailService service;

    @Mock private SendWithUs client;

    private SendWithUsSendRequest request;

    @Before
    public void setup() throws Exception {
        Mockito.when(client.send(Mockito.any(SendWithUsSendRequest.class))).thenAnswer(invocation -> {
            request = invocation.getArgumentAt(0, SendWithUsSendRequest.class);
            return null;
        });

        service = new SendWithUsMailService(client);
    }

    @Test
    public void testSendEmailWithNoData() throws Exception {
        SendEmailRequest emailRequest = new SendEmailRequest();

        service.sendEmail(emailRequest);

        Assert.assertNotNull(request);

        Map<String, Object> map = request.asMap();
        Assert.assertEquals(3, map.size());
        Assert.assertTrue(map.keySet().containsAll(Sets.newHashSet("email_id", "recipient", "email_data")));
        Assert.assertNull(map.get("email_id"));
        Assert.assertNull(map.get("recipient"));
        Assert.assertEquals(0, ((Map) map.get("email_data")).size());
    }

    @Test
    public void testSendEmailWithRecipientName() throws Exception {
        service.sendEmails(Lists.newArrayList(getRequest(true)));
        validateSendEmailOutcome(true);
    }

    @Test
    public void testSendEmailWithoutRecipientName() throws Exception {
        service.sendEmails(Lists.newArrayList(getRequest(false)));
        validateSendEmailOutcome(false);
    }

    @Test(expected = RuntimeException.class)
    public void testSendEmailThrowsException() throws SendWithUsException {
        Mockito.when(client.send(Mockito.any(SendWithUsSendRequest.class))).thenThrow(new SendWithUsException("This is silly!"));
        service.sendEmail(new SendEmailRequest());
    }

    private void validateSendEmailOutcome(boolean includeName) throws Exception {
        Assert.assertNotNull(request);

        Map<String, Object> map = request.asMap();
        Assert.assertEquals(4, map.size());
        Assert.assertTrue(map.keySet().containsAll(Sets.newHashSet("email_id", "recipient", "bcc", "email_data")));
        Assert.assertEquals("tem_12345", map.get("email_id"));

        Map<String, Object> recipient = (Map) map.get("recipient");
        if (includeName) {
            Assert.assertEquals(2, recipient.size());
            Assert.assertTrue(recipient.keySet().containsAll(Sets.newHashSet("name", "address")));
            Assert.assertEquals("steve steverino", recipient.get("name"));
        } else {
            Assert.assertEquals(1, recipient.size());
            Assert.assertTrue(recipient.keySet().containsAll(Sets.newHashSet("address")));
            Assert.assertNull(recipient.get("name"));
        }

        Assert.assertEquals("steve@email.com", recipient.get("address"));

        Map<String, Object> emailData = (Map) map.get("email_data");
        Assert.assertEquals(3, emailData.size());
        Assert.assertTrue(emailData.keySet().containsAll(Sets.newHashSet("first", "second", "third")));
        Assert.assertEquals("1", emailData.get("first"));
        Assert.assertEquals("2", emailData.get("second"));
        Assert.assertEquals("3", emailData.get("third"));

        Map<String, Object>[] bcc = (Map[]) map.get("bcc");
        Assert.assertEquals(2, bcc.length);

        Assert.assertEquals(1, bcc[0].size());
        Assert.assertTrue(bcc[0].keySet().containsAll(Sets.newHashSet("address")));
        Assert.assertEquals("kevin@email.com", bcc[0].get("address"));

        Assert.assertEquals(1, bcc[1].size());
        Assert.assertTrue(bcc[1].keySet().containsAll(Sets.newHashSet("address")));
        Assert.assertEquals("todd@salad.com", bcc[1].get("address"));
    }

    private SendEmailRequest getRequest(boolean includeName) {
        SendEmailRequest emailRequest = new SendEmailRequest();

        emailRequest.setTemplateId("tem_12345");
        emailRequest.setRecipientEmail("steve@email.com");

        if (includeName) {
            emailRequest.setRecipientName("steve steverino");
        }

        emailRequest.addBccEmail("kevin@email.com");
        emailRequest.addBccEmail("todd@salad.com");
        emailRequest.addRequestData("first", "1");
        emailRequest.addRequestData("second", "2");
        emailRequest.addRequestData("third", "3");

        return emailRequest;
    }
}
