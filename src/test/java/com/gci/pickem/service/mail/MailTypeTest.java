package com.gci.pickem.service.mail;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class MailTypeTest {

    @Test
    public void testAllTypesHaveTemplateIdsDefined() {
        for (MailType mailType : MailType.values()) {
            Assert.assertTrue(StringUtils.isNotBlank(mailType.getTemplateId()));
        }
    }
}
