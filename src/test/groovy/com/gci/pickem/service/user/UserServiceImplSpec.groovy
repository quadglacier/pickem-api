package com.gci.pickem.service.user

import com.gci.pickem.data.UserEntity
import com.gci.pickem.data.UserRoleEntity
import com.gci.pickem.model.UserCreationRequest
import com.gci.pickem.model.UserView
import com.gci.pickem.repository.PickViewGrantRepository
import com.gci.pickem.repository.PoolRepository
import com.gci.pickem.repository.UserPoolRepository
import com.gci.pickem.repository.UserRepository
import com.gci.pickem.repository.UserRoleRepository
import com.gci.pickem.service.mail.MailService
import com.gci.pickem.service.mail.SendEmailRequest
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import spock.lang.Specification
import spock.lang.Subject

class UserServiceImplSpec extends Specification {

    @Subject
    UserService userService

    UserRepository userRepository = Mock(UserRepository.class)
    UserRoleRepository userRoleRepository = Mock(UserRoleRepository.class)
    PoolRepository poolRepository = Mock(PoolRepository.class)
    UserPoolRepository userPoolRepository = Mock(UserPoolRepository.class)
    PickViewGrantRepository pickViewGrantRepository = Mock(PickViewGrantRepository.class)
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder()
    MailService mailService = Mock(MailService.class)

    UserEntity savedUser
    UserRoleEntity savedRole
    SendEmailRequest emailRequest

    def setup() {
        userService =
            new UserServiceImpl(
                userRepository,
                userRoleRepository,
                poolRepository,
                userPoolRepository,
                pickViewGrantRepository,
                passwordEncoder,
                mailService)
    }

    def "Return null when user not found" () {
        given:
            userRepository.findOne(1L) >> null

        when:
            UserView user = userService.getUserById(1L)

        then:
            user == null
    }

    def "Return correct user view object for getUserById" () {
        given:
            userRepository.findOne(1L) >>
                new UserEntity(
                    userId: 1L,
                    firstName: 'Steve',
                    lastName: 'Steverson',
                    email: 'steve@email.com',
                    password: 'password',
                    registrationCode: '12345')

        when:
            UserView user = userService.getUserById(1L)

        then:
            user.id == 1L
            user.firstName == 'Steve'
            user.lastName == 'Steverson'
            user.username == 'steve@email.com'
            user.password == '[PROTECTED]'
    }

    def "test create user request missing first name invalid" () {
        when:
            userService.createUser(new UserCreationRequest(lastName: 'Steverson', email: 'steve@email.com'))

        then:
            Exception ex = thrown()
            ex.message == 'User registration requests require a first name, last name, and username'
    }

    def "test create user request missing last name invalid" () {
        when:
            userService.createUser(new UserCreationRequest(firstName: 'Steve', email: 'steve@email.com'))

        then:
            Exception ex = thrown()
            ex.message == 'User registration requests require a first name, last name, and username'
    }

    def "test create user request missing email invalid" () {
        when:
            userService.createUser(new UserCreationRequest(firstName: 'Steve', lastName: 'Steverson'))

        then:
            Exception ex = thrown()
            ex.message == 'User registration requests require a first name, last name, and username'
    }

    def "Create user throws when user exists with email already" () {
        given:
            userRepository.findByEmail('steve@email.com') >> Optional.of(new UserEntity())

        when:
            userService.createUser(
                new UserCreationRequest(
                    firstName: 'Steve',
                    lastName: 'Steverson',
                    email: 'steve@email.com'))

        then:
            Exception ex = thrown()
            ex.message == 'User with email steve@email.com already exists.'
    }

    def "Create user happy path" () {
        given:
            userRepository.findByEmail('steve@email.com') >> Optional.empty()
            userRepository.save(_ as UserEntity) >> { UserEntity user ->
                user.userId = 1L
                savedUser = user
            }

            userRoleRepository.save(_ as UserRoleEntity) >> { UserRoleEntity userRole ->
                userRole.userRoleId = 1L
                savedRole = userRole
            }
            mailService.sendEmail(_ as SendEmailRequest) >> { SendEmailRequest request -> emailRequest = request }

        when:
            userService.createUser(
                new UserCreationRequest(
                    firstName: 'Steve',
                    lastName: 'Steverson',
                    email: 'steve@email.com',
                    clientUrl: 'https://www.test.com/'))

        then:
            savedUser != null
            savedUser.userId == 1L
            savedUser.firstName == 'Steve'
            savedUser.lastName == 'Steverson'
            savedUser.email == 'steve@email.com'
            savedUser.password == null
            savedUser.registrationCode != null
            savedUser.registrationCode.length() == 32
            savedUser.registrationCode.count('-') == 0

            savedRole != null
            savedRole.userRoleId == 1L
            savedRole.userId == 1L
            savedRole.role == 'USER'

            emailRequest != null
            emailRequest.templateId == 'tem_tbk79DqGrvWpfrM7JQTT8RP8'
            emailRequest.recipientName == 'Steve'
            emailRequest.recipientEmail == 'steve@email.com'

            emailRequest.requestData.size() == 3
            emailRequest.requestData.get('nonce') == savedUser.registrationCode
            emailRequest.requestData.get('clientUrl') == 'https://www.test.com/'
            emailRequest.requestData.get('firstName') == 'Steve'
    }
}
