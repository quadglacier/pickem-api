create table pick_view_grants (
  pick_view_grant_id BIGSERIAL PRIMARY KEY NOT NULL,
  granting_user_id int NOT NULL REFERENCES users(user_id),
  granted_pool_id int NOT NULL REFERENCES pool(pool_id),
  granted_user_id int NOT NULL REFERENCES users(user_id)
);