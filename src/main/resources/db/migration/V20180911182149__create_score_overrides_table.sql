create table user_score_override (
  user_score_override_id BIGSERIAL PRIMARY KEY NOT NULL,
  user_id int NOT NULL REFERENCES users(user_id),
  pool_id int NOT NULL REFERENCES pool(pool_id),
  authorizing_user_id int NOT NULL REFERENCES users(user_id),
  season INTEGER NOT NULL,
  week INTEGER NOT NULL,
  score INTEGER NOT NULL
);