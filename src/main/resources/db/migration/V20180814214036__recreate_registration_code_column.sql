alter table users add column reg_code_tmp varchar(256);

update users set reg_code_tmp = "registration_code";

alter table users drop column "registration_code";

alter table users add column registration_code varchar(256);

update users set registration_code = reg_code_tmp;

alter table users drop column reg_code_tmp;