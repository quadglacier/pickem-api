create table job_run_times (
  job_run_time_id BIGSERIAL PRIMARY KEY NOT NULL,
  schedule_updater BIGINT NOT NULL DEFAULT 0,
  pick_notifier BIGINT NOT NULL DEFAULT 0
);

insert into job_run_times (schedule_updater, pick_notifier) values (0, 0);