package com.gci.pickem.service.scoring;

import com.gci.pickem.model.ScoreResponse;

public interface ScoringService {

    ScoreResponse getScore(long userId, long poolId, int season, int week);

    ScoreResponse getWeekScores(long userId, long poolId, int season, int week);

    ScoreResponse getSeasonScores(long userId, long poolId, int season);

    void overrideUserScore(long authorizingUserId, long userId, long poolId, int season, int week, int score);
}
