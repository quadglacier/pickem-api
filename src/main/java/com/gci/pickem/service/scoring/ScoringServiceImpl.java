package com.gci.pickem.service.scoring;

import com.gci.pickem.data.*;
import com.gci.pickem.exception.InvalidScoringRequestException;
import com.gci.pickem.exception.InvalidUserPoolException;
import com.gci.pickem.exception.MissingRequiredDataException;
import com.gci.pickem.exception.notfound.UserNotFoundException;
import com.gci.pickem.model.ScoreResponse;
import com.gci.pickem.model.UserPoolRole;
import com.gci.pickem.model.UserScoreView;
import com.gci.pickem.repository.*;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ScoringServiceImpl implements ScoringService {
    private static final Logger log = LoggerFactory.getLogger(ScoringServiceImpl.class);

    private UserRepository userRepository;
    private PickRepository pickRepository;
    private GameRepository gameRepository;
    private UserPoolRepository userPoolRepository;
    private UserScoreOverrideRepository userScoreOverrideRepository;

    @Autowired
    ScoringServiceImpl(
        UserRepository userRepository,
        PickRepository pickRepository,
        GameRepository gameRepository,
        UserPoolRepository userPoolRepository,
        UserScoreOverrideRepository userScoreOverrideRepository
    ) {
        this.userRepository = userRepository;
        this.pickRepository = pickRepository;
        this.gameRepository = gameRepository;
        this.userPoolRepository = userPoolRepository;
        this.userScoreOverrideRepository = userScoreOverrideRepository;
    }

    @Override
    @Transactional
    public ScoreResponse getScore(long userId, long poolId, int season, int week) {
        validateUserIsValidForPoolRequest(userId, poolId);

        // Retrieve the user's picks for the week (and this pool!)
        Set<UserScoreOverrideEntity> scoreOverrides =
            userScoreOverrideRepository.findByUserIdAndPoolIdAndSeason(userId, poolId, season);
        Set<PickEntity> picks = pickRepository.getPicks(userId, poolId, season);

        UserEntity user = userRepository.findOne(userId);

        UserScoreView view = new UserScoreView();
        view.setFirstName(user.getFirstName());
        view.setLastName(user.getLastName());
        view.setUsername(user.getEmail());
        view.setWeekScore(getWeekScore(picks, scoreOverrides, week));
        view.setWeekPossiblePointsRemaining(getPossiblePointsRemainingForPicks(picks, week));
        view.setYtdScore(getSeasonScore(picks, scoreOverrides, week));

        return getResponse(poolId, season, Lists.newArrayList(view), week);
    }

    @Override
    public ScoreResponse getWeekScores(long userId, long poolId, int season, int week) {
        validateUserIsValidForPoolRequest(userId, poolId);

        Set<PickEntity> poolPicks = pickRepository.getPicks(poolId, season);

        List<UserScoreView> scores = getScores(poolPicks, poolId, season, week);

        scores.sort(((o1, o2) -> Integer.compare(o2.getWeekScore(), o1.getWeekScore())));

        return getResponse(poolId, season, scores, week);
    }

    @Override
    public ScoreResponse getSeasonScores(long userId, long poolId, int season) {
        validateUserIsValidForPoolRequest(userId, poolId);

        Set<PickEntity> poolPicks = pickRepository.getPicks(poolId, season);

        int currentWeek = gameRepository.getCurrentWeek(Instant.now().toEpochMilli());
        List<UserScoreView> scores = getScores(poolPicks, poolId, season, currentWeek);

        scores.sort(((o1, o2) -> Integer.compare(o2.getYtdScore(), o1.getYtdScore())));

        // No week is specified, we want the full season!
        return getResponse(poolId, season, scores, currentWeek);
    }

    @Override
    @Transactional
    public void overrideUserScore(long authorizingUserId, long userId, long poolId, int season, int week, int score) {
        validateUserIsValidForPoolRequest(authorizingUserId, poolId);
        validateUserIsValidForPoolRequest(userId, poolId);

        // Authorizing user has to be a pool admin.
        UserPoolEntity authorizingUserPool =
            userPoolRepository.findByUserId(authorizingUserId)
                .stream()
                .filter(userPool -> userPool.getPoolId().equals(poolId))
                .findFirst()
                .orElse(null);

        if (authorizingUserPool == null) {
            throw new AccessDeniedException(String.format("User with ID %d doesn't belong to pool %d", authorizingUserId, poolId));
        } else if (!UserPoolRole.ADMIN.name().equals(authorizingUserPool.getUserRole())) {
            throw new AccessDeniedException(String.format("User with ID %d is not an admin of pool %d", authorizingUserId, poolId));
        }

        // Everything is valid, nuke the user's picks for the week and create the override.
        UserScoreOverrideEntity override =
            userScoreOverrideRepository.findByUserIdAndPoolIdAndSeasonAndWeek(userId, poolId, season, week);
        if (override == null) {
            override = new UserScoreOverrideEntity();
            override.setAuthorizingUserId(authorizingUserId);
            override.setUserId(userId);
            override.setPoolId(poolId);
            override.setSeason(season);
            override.setWeek(week);
        } else {
            log.info(
                "Changing previously overriden score for user {}, pool {}, season {}, and week {} from {} to {}",
                userId,
                poolId,
                season,
                week,
                override.getScore(),
                score);
        }

        override.setScore(score);

        userScoreOverrideRepository.save(override);

        Set<PickEntity> picks = pickRepository.getPicks(userId, poolId, season, week);
        pickRepository.delete(picks);
    }

    private ScoreResponse getResponse(long poolId, int season, List<UserScoreView> userScoreViews, Integer week) {
        ScoreResponse scoreResponse = new ScoreResponse();

        scoreResponse.setPoolId(poolId);
        scoreResponse.setSeason(season);
        if (week != null) {
            scoreResponse.setWeek(week);
        }

        scoreResponse.setUserScores(userScoreViews);

        return scoreResponse;
    }

    private List<UserScoreView> getScores(Set<PickEntity> picks, long poolId, int season, int week) {
        Map<Long, List<PickEntity>> userPicksMultiMap = new HashMap<>();

        // Initialize the pool to have an entry for each user.
        Set<UserPoolEntity> userPools = userPoolRepository.findByPoolId(poolId);

        userPools.forEach(userPool -> userPicksMultiMap.put(userPool.getUserId(), new ArrayList<>()));

        picks.forEach(pick -> userPicksMultiMap.get(pick.getUserId()).add(pick));

        List<UserScoreView> results = new ArrayList<>();
        for (Map.Entry<Long, List<PickEntity>> entry : userPicksMultiMap.entrySet()) {
            Collection<PickEntity> value = entry.getValue();

            UserEntity user = userRepository.findOne(entry.getKey());

            UserScoreView view = new UserScoreView();

            view.setFirstName(user.getFirstName());
            view.setLastName(user.getLastName());
            view.setUsername(user.getEmail());

            Set<UserScoreOverrideEntity> overrides =
                userScoreOverrideRepository.findByUserIdAndPoolIdAndSeason(user.getUserId(), poolId, season);

            view.setWeekScore(getWeekScore(value, overrides, week));
            view.setWeekPossiblePointsRemaining(getPossiblePointsRemainingForPicks(value, week));
            view.setYtdScore(getSeasonScore(value, overrides, week));

            results.add(view);
        }

        return results;
    }

    private void validateUserIsValidForPoolRequest(long userId, long poolId) {
        UserEntity user = userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException(userId);
        }

        // Verify user is a member of the specified pool.
        Set<Long> userPoolIds = user.getUserPools().stream().map(UserPoolEntity::getPoolId).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(userPoolIds) || !userPoolIds.contains(poolId)) {
            throw new InvalidUserPoolException(String.format("User with ID %d does not belong to pool with ID %d", userId, poolId));
        }
    }

    private int getPossiblePointsRemainingForPicks(Collection<PickEntity> picks, int week) {
        return picks.stream()
            .filter(pick -> pick.getGame().getWeek() == week)
            .filter(pick -> !pick.getGame().getGameComplete())
            .filter(pick -> pick.getChosenTeamId() != null && pick.getConfidence() != null)
            .mapToInt(PickEntity::getConfidence)
            .sum();
    }

    private int getScoreForPicks(Collection<PickEntity> picks, Collection<UserScoreOverrideEntity> overrides) {
        // For each pick, check against the pool's strategy to see how many picks the user got right and calculate the score.
        int score = 0;

        for (UserScoreOverrideEntity override : overrides) {
            // Start with the override scores.
            score += override.getScore();
        }

        for (PickEntity pick : picks) {
            if (!isOverridden(pick, overrides)) {
                try {
                    score += getScoreForPick(pick);
                } catch (InvalidScoringRequestException e) {
                    // This is okay, the game just isn't complete yet.
                    log.trace("", e);
                }
            }
        }

        return score;
    }

    private boolean isOverridden(PickEntity pick, Collection<UserScoreOverrideEntity> overrides) {
        UserScoreOverrideEntity overrideMatch =
            overrides.stream()
                .filter(override -> override.getSeason().equals(pick.getGame().getSeason()))
                .filter(override -> override.getWeek().equals(pick.getGame().getWeek()))
                .filter(override -> override.getUserId().equals(pick.getUserId()))
                .filter(override -> override.getPoolId().equals(pick.getPoolId()))
                .findFirst()
                .orElse(null);

        return overrideMatch != null;
    }

    private int getScoreForPick(PickEntity pick) {
        if (pick == null) {
            throw new MissingRequiredDataException("Pick must not be null in order to calculate score");
        }

        if (pick.getGame() == null) {
            throw new MissingRequiredDataException(String.format("No associated game found for pick with ID %d", pick.getPickId()));
        }

        GameEntity game = pick.getGame();
        if (!game.getGameComplete()) {
            throw new InvalidScoringRequestException(String.format("Game with ID %d is not complete. Unable to calculate score.", game.getGameId()));
        }

        if (pick.getChosenTeamId() == null || !pick.getChosenTeamId().equals(game.getWinningTeamId())) {
            // User didn't make a pick or the pick was wrong. Goose egg for you.
            return 0;
        } else {
            return pick.getConfidence();
        }
    }

    private int getWeekScore(Collection<PickEntity> picks, Collection<UserScoreOverrideEntity> scoreOverrides, int week) {
        return
            getScoreForPicks(
                picks.stream()
                    .filter(pick -> pick.getGame().getWeek() == week)
                    .collect(Collectors.toList()),
                scoreOverrides.stream()
                    .filter(override -> override.getWeek().equals(week))
                    .collect(Collectors.toSet()));
    }

    private int getSeasonScore(Collection<PickEntity> picks, Collection<UserScoreOverrideEntity> scoreOverrides, int week) {
        return
            getScoreForPicks(
                picks.stream()
                    .filter(pick -> pick.getGame().getWeek() <= week)
                    .collect(Collectors.toList()),
                scoreOverrides.stream()
                    .filter(override -> override.getWeek() <= week)
                    .collect(Collectors.toSet()));
    }
}
