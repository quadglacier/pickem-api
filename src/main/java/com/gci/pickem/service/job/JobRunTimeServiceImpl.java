package com.gci.pickem.service.job;

import com.gci.pickem.data.JobRunTimeEntity;
import com.gci.pickem.repository.JobRunTimeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobRunTimeServiceImpl implements JobRunTimeService {
    private static final Logger log = LoggerFactory.getLogger(JobRunTimeServiceImpl.class);

    private JobRunTimeRepository jobRunTimeRepository;

    @Autowired
    public JobRunTimeServiceImpl(JobRunTimeRepository jobRunTimeRepository) {
        this.jobRunTimeRepository = jobRunTimeRepository;
    }

    @Override
    public Long getLastPickNotifierRunTime() {
        return getJobRunTimeEntry().getPickNotifierEpoch();
    }

    @Override
    public void updateLastPickNotifierRunTime(long runTimeEpoch) {
        JobRunTimeEntity jobRunTime = getJobRunTimeEntry();
        jobRunTime.setPickNotifierEpoch(runTimeEpoch);

        jobRunTimeRepository.save(jobRunTime);
    }

    @Override
    public Long getLastScheduleUpdaterRunTime() {
        return getJobRunTimeEntry().getScheduleUpdaterEpoch();
    }

    @Override
    public void updateLastScheduleUpdaterRunTime(long runTimeEpoch) {
        JobRunTimeEntity jobRunTime = getJobRunTimeEntry();
        jobRunTime.setScheduleUpdaterEpoch(runTimeEpoch);

        jobRunTimeRepository.save(jobRunTime);
    }

    private JobRunTimeEntity getJobRunTimeEntry() {
        List<JobRunTimeEntity> all = jobRunTimeRepository.findAll();
        if (all.size() != 1) {
            log.warn("Unexpected number of job run time entries {} encountered!", all.size());

            if (all.isEmpty()) {
                log.warn("Creating new Job Run Time entry since none was found.");
                JobRunTimeEntity jobRunTime = new JobRunTimeEntity();
                jobRunTime.setPickNotifierEpoch(0L);
                jobRunTime.setScheduleUpdaterEpoch(0L);

                return jobRunTimeRepository.save(jobRunTime);
            }
        }

        return all.get(0);
    }
}
