package com.gci.pickem.service.job;

public interface JobRunTimeService {

    Long getLastScheduleUpdaterRunTime();

    void updateLastScheduleUpdaterRunTime(long runTimeEpoch);

    Long getLastPickNotifierRunTime();

    void updateLastPickNotifierRunTime(long runTimeEpoch);
}
