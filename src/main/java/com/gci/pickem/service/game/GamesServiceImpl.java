package com.gci.pickem.service.game;

import com.gci.pickem.data.GameEntity;
import com.gci.pickem.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class GamesServiceImpl implements GamesService {
    private static final Logger log = LoggerFactory.getLogger(GamesServiceImpl.class);

    private GameRepository gameRepository;

    @Autowired
    GamesServiceImpl(
        GameRepository gameRepository
    ) {
        this.gameRepository = gameRepository;
    }

    @Override
    public GameEntity findByExternalId(Integer externalId) {
       return gameRepository.findByExternalId(externalId);
    }

    @Override
    @Transactional
    public GameEntity saveGame(GameEntity game) {
        log.info("Saving game to database with external ID {}", game.getExternalId());
        return gameRepository.save(game);
    }

    @Override
    public Collection<GameEntity> findAllBySeasonAndWeek(int season, int week) {
        return gameRepository.findAllBySeasonAndWeek(season, week);
    }
}
