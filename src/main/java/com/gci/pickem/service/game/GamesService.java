package com.gci.pickem.service.game;

import com.gci.pickem.data.GameEntity;

import java.util.Collection;

public interface GamesService {

    GameEntity findByExternalId(Integer externalId);

    GameEntity saveGame(GameEntity game);

    Collection<GameEntity> findAllBySeasonAndWeek(int season, int week);
}
