package com.gci.pickem.service.schedule;

import com.gci.pickem.converter.TeamConverter;
import com.gci.pickem.data.GameEntity;
import com.gci.pickem.data.TeamEntity;
import com.gci.pickem.exception.notfound.GameNotFoundException;
import com.gci.pickem.exception.notfound.TeamNotFoundException;
import com.gci.pickem.model.GamesList;
import com.gci.pickem.model.TeamView;
import com.gci.pickem.model.mysportsfeeds.BaseTeam;
import com.gci.pickem.model.mysportsfeeds.v2.*;
import com.gci.pickem.service.game.GamesService;
import com.gci.pickem.service.mysportsfeeds.v2.MySportsFeedsV2Service;
import com.gci.pickem.converter.GameConverter;
import com.gci.pickem.service.team.TeamService;
import com.gci.pickem.util.PickEmConstants;
import com.gci.pickem.util.ScheduleUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl implements ScheduleService {
    private static final Logger log = LoggerFactory.getLogger(ScheduleServiceImpl.class);

    private MySportsFeedsV2Service mySportsFeedsV2Service;
    private TeamService teamService;
    private GamesService gamesService;
    private GameConverter gameConverter;
    private TeamConverter teamConverter;

    @Autowired
    ScheduleServiceImpl(
        MySportsFeedsV2Service mySportsFeedsV2Service,
        TeamService teamService,
        GamesService gamesService,
        GameConverter gameConverter,
        TeamConverter teamConverter
    ) {
        this.mySportsFeedsV2Service = mySportsFeedsV2Service;
        this.teamService = teamService;
        this.gamesService = gamesService;
        this.gameConverter = gameConverter;
        this.teamConverter = teamConverter;
    }

    @Override
    @Transactional
    public void processExternalGamesForNextDays(int days) {
        GamesResponse response = mySportsFeedsV2Service.getGamesUntilDaysFromNow(days);
        if (CollectionUtils.isNotEmpty(response.getGames())) {
            log.debug("Found {} games on the schedule. for the next {} days", response.getGames().size(), days);
            // Process the games if we don't already have them.
            processExternalGames(response);
        } else {
            log.debug("No scheduled games found for today.");
        }
    }

    @Override
    @Transactional
    public GamesList getGamesForSeasonAndWeek(int season, int week) {
        synchronized (this) {
            // Don't let more than one thread in here at a time!
            Collection<GameEntity> existingGames = gamesService.findAllBySeasonAndWeek(season, week);
            if (CollectionUtils.isEmpty(existingGames)) {
                // Games don't yet exist. Grab the data from MSF.
                GamesResponse gamesForSeasonAndWeek = mySportsFeedsV2Service.getGamesForSeasonAndWeek(season, week);

                List<GameEntity> newGames = processExternalGames(gamesForSeasonAndWeek);

                log.info("Received {} games from external resource", newGames.size());

                List<com.gci.pickem.model.Game> games =
                    newGames.stream()
                        .map(this::getGameView)
                        .sorted(Comparator.comparing(com.gci.pickem.model.Game::getGameTimeEpoch))
                        .collect(Collectors.toList());

                return new GamesList(games);
            } else {
                return new GamesList(
                    existingGames.stream()
                        .map(this::getGameView)
                        .sorted(Comparator.comparing(com.gci.pickem.model.Game::getGameTimeEpoch))
                        .collect(Collectors.toList()));
            }
        }
    }

    private GameEntity getGameByExternalId(int externalId, int week, Instant date) {
        GameEntity game = gamesService.findByExternalId(externalId);
        if (game == null) {
            // We have a score, but not the actual game somehow... Request the week's games to fill in.
            getGamesForSeasonAndWeek(ScheduleUtil.getSeasonForDate(date), week);

            // Try to get it again.
            game = gamesService.findByExternalId(externalId);
            if (game == null) {
                throw new GameNotFoundException(
                        String.format("Unable to find game with external ID %d in database.", externalId));
            }
        }

        return game;
    }

    @Override
    @Transactional
    public void processScoresForDate(Instant date) {
        GamesResponse response = mySportsFeedsV2Service.getFinalGamesForDate(date);
        if (response == null || CollectionUtils.isEmpty(response.getGames())) {
            log.info("No final scores found for date {}", date.atZone(ZoneId.of(PickEmConstants.MSF_TIMEZONE)));
            return;
        }

        response.getGames().forEach(game -> {
            try {
                Schedule schedule = game.getSchedule();
                if (PlayedStatus.COMPLETED != schedule.getPlayedStatus()) {
                    log.warn("Received non-complete game in response for game with external ID {}", schedule.getId());
                    return;
                }

                GameEntity entity = getGameByExternalId(schedule.getId(), schedule.getWeek(), date);
                if (entity.getGameComplete()) {
                    // Game has already been processed. Nothing to do.
                    log.info("Game with ID {} has already been marked as complete. Skipping.", entity.getGameId());
                    return;
                }

                Score score = game.getScore();
                int home = score.getHomeScoreTotal();
                int away = score.getAwayScoreTotal();

                if (home != away) {
                    // There is a winner. Sad day.
                    entity.setWinningTeamId(getWinningTeamId(home, away, schedule, entity.getGameId()));
                }

                entity.setGameComplete(true);

                gamesService.saveGame(entity);
            } catch (Exception e) {
                log.error("Error occurred while attempting to process game score: {}", e.getMessage());
            }
        });
    }

    private long getWinningTeamId(int homeScore, int awayScore, Schedule schedule, Long gameId) {
        int winningTeamId =
            homeScore > awayScore ?
                schedule.getHomeTeam().getId() :
                schedule.getAwayTeam().getId();

        TeamEntity team = teamService.findByExternalId((long) winningTeamId);
        if (team == null) {
            throw new TeamNotFoundException(String.format("No team found for external ID %d", winningTeamId));
        }

        log.info("Game with ID {} is complete with winning team ID {}.", gameId, team.getTeamId());

        return team.getTeamId();
    }

    private com.gci.pickem.model.Game getGameView(GameEntity game) {
        com.gci.pickem.model.Game model = new com.gci.pickem.model.Game();

        model.setGameId(game.getGameId());
        model.setGameTimeEpoch(game.getGameTimeEpoch());

        TeamView homeTeam = new TeamView(teamService.findById(game.getHomeTeamId()));
        model.setHomeTeam(homeTeam);

        TeamView awayTeam = new TeamView(teamService.findById(game.getAwayTeamId()));
        model.setAwayTeam(awayTeam);

        model.setGameComplete(game.getGameComplete());
        if (game.getGameComplete() && game.getWinningTeamId() != null) {
            model.setWinningTeam(
                game.getWinningTeamId().equals(game.getHomeTeamId()) ?
                    homeTeam :
                    awayTeam);
        }

        model.setGameTime(LocalDateTime.ofInstant(Instant.ofEpochMilli(game.getGameTimeEpoch()), ZoneId.of(PickEmConstants.MSF_TIMEZONE)));

        return model;
    }

    private List<GameEntity> processExternalGames(GamesResponse gamesResponse) {
        List<GameEntity> games = new ArrayList<>();

        for (com.gci.pickem.model.mysportsfeeds.v2.Game entry : gamesResponse.getGames()) {
            // Process teams before the game.
            TeamReference homeTeam = getTeamReferenceForGame(gamesResponse, entry.getSchedule().getHomeTeam().getId());
            TeamReference awayTeam = getTeamReferenceForGame(gamesResponse, entry.getSchedule().getAwayTeam().getId());

            processTeam(homeTeam);
            processTeam(awayTeam);

            games.add(processGame(entry));
        }

        return games;
    }

    private TeamReference getTeamReferenceForGame(GamesResponse gamesResponse, Integer teamId) {
        return Optional.ofNullable(gamesResponse.getReferences().getTeamReferences())
            .orElse(new ArrayList<>())
            .stream()
            .filter(teamRef -> teamRef.getId().equals(teamId))
            .findFirst()
            .orElse(null);
    }

    private GameEntity processGame(com.gci.pickem.model.mysportsfeeds.v2.Game entry) {
        GameEntity game = gamesService.findByExternalId(entry.getSchedule().getId());

        if (game != null) {
            if (game.getAdminOverridden()) {
                log.info("Game with ID {} is admin overridden. Not accepting changes from external source.", game.getGameId());
                return game;
            }

            // It's possible the game has an updated schedule, we should process that too.
            long existingEpoch = game.getGameTimeEpoch();
            long entryEpoch = TimeUnit.SECONDS.toMillis(entry.getSchedule().getStartTime().toEpochSecond());

            if (existingEpoch == entryEpoch) {
                // No further processing to do, use the DB instance of the game.
                return game;
            }

            log.info("Game with ID {} has an updated game time. Was {}, updating to {}.", game.getGameId(), existingEpoch, entryEpoch);
            if (existingEpoch > entryEpoch) {
                // Game was somehow rescheduled earlier. This could be a big problem.
                log.warn("Game with ID {} was rescheduled to an earlier time. This could be a concern for picks.", game.getGameId());
            }

            // Set the new game time and continue on to update in the DB.
            game.setGameTimeEpoch(entryEpoch);
        } else {
            // Game isn't in the DB yet.
            log.info("Game with external ID {} does not exist in the database yet. Converting.", entry.getSchedule().getId());
            game = gameConverter.convert(entry);
        }

        // This needs to be transactional here!
        return gamesService.saveGame(game);
    }

    private void processTeam(BaseTeam external) {
        if (external == null) {
            return;
        }

        TeamEntity team = teamService.findByExternalId((long) external.getId());
        if (team == null) {
            // Team doesn't already exist.
            team = new TeamEntity();
            team.setAbbreviation(external.getAbbreviation());
            team.setCity(external.getCity());
            team.setTeamName(external.getName());
            team.setExternalId((long) external.getId());

            // This needs to be transactional here!
            teamService.upsertTeam(team);
        } else {
            TeamEntity externalView = teamConverter.convert(external);
            if (!team.equals(externalView)) {
                log.info("Difference in team definition found for team with ID {}: Current entity: {} - New entity: {} ", team.getTeamId(), team, externalView);
                externalView.setTeamId(team.getTeamId());
                teamService.upsertTeam(externalView);
            }
        }
    }
}
