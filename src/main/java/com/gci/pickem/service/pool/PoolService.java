package com.gci.pickem.service.pool;

import com.gci.pickem.model.*;

import java.util.List;

public interface PoolService {

    PoolView createPool(long userId, PoolView poolView);

    // Returns email addresses that had failures.
    List<String> sendPoolInvites(long userId, long poolId, List<String> emails, String clientUrl);

    void processPoolInviteResponse(long userId, long poolId, String inviteAction);

    List<PoolInviteView> getPoolInvitesForPool(long userId, long poolId);

    List<PoolInviteView> getPoolInvitesForUser(long userId);

    UserPoolListResponse getPoolsForUser(long userId);

    PoolUserListResponse getUsersForPool(long poolId, long userId);

    void sendPoolMessage(long userId, long poolId, String message);
}
