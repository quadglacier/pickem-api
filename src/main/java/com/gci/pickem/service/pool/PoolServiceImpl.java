package com.gci.pickem.service.pool;

import com.gci.pickem.data.PoolEntity;
import com.gci.pickem.data.PoolInviteEntity;
import com.gci.pickem.data.UserEntity;
import com.gci.pickem.data.UserPoolEntity;
import com.gci.pickem.exception.notfound.PoolNotFoundException;
import com.gci.pickem.exception.notfound.UserNotFoundException;
import com.gci.pickem.model.*;
import com.gci.pickem.repository.PoolInviteRepository;
import com.gci.pickem.repository.PoolRepository;
import com.gci.pickem.repository.UserPoolRepository;
import com.gci.pickem.repository.UserRepository;
import com.gci.pickem.service.mail.MailService;
import com.gci.pickem.service.mail.MailType;
import com.gci.pickem.service.mail.SendEmailRequest;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PoolServiceImpl implements PoolService {
    private static final Logger log = LoggerFactory.getLogger(PoolServiceImpl.class);

    private PoolRepository poolRepository;
    private UserPoolRepository userPoolRepository;
    private UserRepository userRepository;
    private PoolInviteRepository poolInviteRepository;
    private MailService mailService;

    @Autowired
    PoolServiceImpl(
       PoolRepository poolRepository,
       UserPoolRepository userPoolRepository,
       UserRepository userRepository,
       PoolInviteRepository poolInviteRepository,
       MailService mailService
    ) {
        this.poolRepository = poolRepository;
        this.userPoolRepository = userPoolRepository;
        this.userRepository = userRepository;
        this.poolInviteRepository = poolInviteRepository;
        this.mailService = mailService;
    }

    @Override
    @Transactional
    public PoolView createPool(long userId, PoolView poolView) {
        UserEntity user = userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException(userId);
        }

        if (StringUtils.isBlank(poolView.getName()) || StringUtils.isBlank(poolView.getClientUrl()) || poolView.getScoringMethod() == null) {
            throw new RuntimeException("Pool creation requests require a name, client URL, and scoring method.");
        }

        // Create the pool.
        PoolEntity pool = poolRepository.save(new PoolEntity(poolView));

        // Associate the creating user with the pool.
        UserPoolEntity userPool = new UserPoolEntity();
        userPool.setUserId(userId);
        userPool.setPoolId(pool.getPoolId());
        userPool.setUserRole(UserPoolRole.ADMIN.name());

        userPoolRepository.save(userPool);

        return new PoolView(pool);
    }

    @Override
    public void processPoolInviteResponse(long userId, long poolId, String inviteAction) {
        UserEntity user = userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException(userId);
        }

        Optional<UserPoolEntity> userPool = user.getUserPools().stream().filter(item -> item.getPoolId().equals(poolId)).findFirst();
        if (userPool.isPresent()) {
            throw new RuntimeException(String.format("User with ID %d already belongs to pool with ID %d", userId, poolId));
        }

        Optional<PoolInviteEntity> result = poolInviteRepository.findByUserIdAndPoolId(userId, poolId);
        if (!result.isPresent()) {
            throw new RuntimeException(String.format("User with ID %d does not have an invite for pool with ID %d", userId, poolId));
        }

        PoolInviteEntity invite = result.get();
        PoolInviteStatus currentStatus = PoolInviteStatus.valueOf(invite.getInviteStatus());
        if (currentStatus != PoolInviteStatus.PENDING) {
            throw new RuntimeException(String.format("Pool invite for user ID %d to pool with ID %d has already been responded to.", userId, poolId));
        }

        PoolInviteStatus newStatus = PoolInviteStatus.valueOf(inviteAction);
        if (newStatus == PoolInviteStatus.ACCEPTED) {
            UserPoolEntity newUserPool = new UserPoolEntity();
            newUserPool.setPoolId(poolId);
            newUserPool.setUserId(userId);

            // User is always a participant when accepting a pool invite.
            newUserPool.setUserRole(UserPoolRole.PARTICIPANT.name());

            userPoolRepository.save(newUserPool);
        }

        invite.setInviteStatus(newStatus.name());

        poolInviteRepository.save(invite);
    }

    @Override
    @Transactional
    public List<PoolInviteView> getPoolInvitesForPool(long userId, long poolId) {
        // Only allow pool invites to be seen by pool admins.
        validateUserIsPoolAdmin(userId, poolId);

        return poolInviteRepository.findByPoolId(poolId).stream().map(PoolInviteView::new).collect(Collectors.toList());
    }

    @Override
    public List<PoolInviteView> getPoolInvitesForUser(long userId) {
        return poolInviteRepository.findByUserId(userId).stream().map(PoolInviteView::new).collect(Collectors.toList());
    }

    @Override
    public List<String> sendPoolInvites(long userId, long poolId, List<String> emails, String clientUrl) {
        validateUserIsPoolAdmin(userId, poolId);

        UserEntity inviter = userRepository.findOne(userId);
        PoolEntity pool = poolRepository.findOne(poolId);

        List<String> errorEmails = new ArrayList<>();

        // User exists, belongs to the pool, and is an admin of the pool. Invites can be sent!
        for (String email : emails) {
            try {
                PoolInviteEntity invite = new PoolInviteEntity();
                invite.setInvitingUserId(userId);
                invite.setPoolId(poolId);
                invite.setInviteeEmail(email);

                poolInviteRepository.save(invite);

                SendEmailRequest request = new SendEmailRequest();
                request.setTemplateId(MailType.POOL_INVITE.getTemplateId());
                request.setRecipientEmail(email);

                request.addRequestData("inviterFirstName", inviter.getFirstName());
                request.addRequestData("inviterLastName", inviter.getLastName());
                request.addRequestData("poolName", pool.getPoolName());
                request.addRequestData("clientUrl", clientUrl);

                mailService.sendEmail(request);

                log.debug("Invite for pool ID {} sent to {}.", poolId, email);
            } catch (Exception e) {
                // Don't let this prevent other invites from being sent.
                errorEmails.add(email);

                log.trace("", e);
                log.error("Error occurred while attempting to send invite to pool ID {} to email {}: {}", poolId, email, e.getMessage());
            }
        }

        return errorEmails;
    }

    @Override
    @Transactional
    public UserPoolListResponse getPoolsForUser(long userId) {
        UserPoolListResponse response = new UserPoolListResponse();

        UserEntity user = userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException(userId);
        }

        response.setFirstName(response.getFirstName());
        response.setLastName(response.getLastName());

        Set<UserPoolEntity> userPools = userPoolRepository.findByUserId(userId);
        if (CollectionUtils.isEmpty(userPools)) {
            response.setPoolViews(new ArrayList<>());
            return response;
        }

        List<UserPoolView> poolViews =
            userPools.stream()
                .map(userPool -> {
                    UserPoolView userPoolView = new UserPoolView();

                    userPoolView.setPoolId(userPool.getPoolId());
                    userPoolView.setPoolName(userPool.getPool().getPoolName());
                    userPoolView.setUserRole(userPool.getUserRole());
                    userPoolView.setPickGracePeriod(userPool.getPool().getPickGracePeriod());

                    return userPoolView;
                })
                .collect(Collectors.toList());

        response.setPoolViews(poolViews);

        return response;
    }

    @Override
    @Transactional
    public PoolUserListResponse getUsersForPool(long poolId, long userId) {
        PoolUserListResponse response = new PoolUserListResponse();

        PoolEntity pool = poolRepository.findOne(poolId);
        if (pool == null) {
            throw new PoolNotFoundException(poolId);
        }

        response.setPoolName(pool.getPoolName());

        Set<UserPoolEntity> userPools = userPoolRepository.findByPoolId(poolId);
        if (CollectionUtils.isEmpty(userPools)) {
            response.setUsers(new ArrayList<>());
            return response;
        }

        Optional<UserPoolEntity> match =
            userPools.stream()
                .filter(userPool -> userPool.getUserId().equals(userId))
                .findFirst();

        if (!match.isPresent()) {
            throw new RuntimeException("Requesting user does not belong to specified pool");
        }

        List<PoolUserView> userViews =
            userPools.stream()
                .sorted((o1, o2) -> {
                    if (o1.getUserRole().equals(o2.getUserRole())) {
                        // Same role, order by name
                        if (o1.getUser().getLastName().equals(o2.getUser().getLastName())) {
                            return o1.getUser().getFirstName().compareTo(o2.getUser().getFirstName());
                        } else {
                            return o1.getUser().getLastName().compareTo(o2.getUser().getLastName());
                        }
                    } else {
                        // Different roles
                        return o1.getUserRole().compareTo(o2.getUserRole());
                    }
                })
                .map(userPool -> {
                    PoolUserView userView = new PoolUserView();
                    userView.setFirstName(userPool.getUser().getFirstName());
                    userView.setLastName(userPool.getUser().getLastName());
                    userView.setPoolRole(userPool.getUserRole());
                    return userView;
                })
                .collect(Collectors.toList());

        response.setUsers(userViews);

        return response;
    }

    @Override
    public void sendPoolMessage(long userId, long poolId, String message) {
        validateUserIsPoolAdmin(userId, poolId);

        UserEntity admin = userRepository.findOne(userId);

        PoolEntity pool = poolRepository.findOne(poolId);
        Set<UserEntity> poolUsers = userRepository.findAllByPoolId(poolId);

        SendEmailRequest messageRequest = new SendEmailRequest();
        messageRequest.setTemplateId(MailType.ADMIN_POOL_MESSAGE.getTemplateId());

        // SWU requires a recipient address, so send to the admin and BCC everyone else.
        messageRequest.setRecipientEmail(admin.getEmail());
        poolUsers.stream()
            .filter(user -> !user.getUserId().equals(admin.getUserId()))
            .forEach(user -> messageRequest.addBccEmail(user.getEmail()));

        messageRequest.addRequestData("messages", getMessages(message));
        messageRequest.addRequestData("poolName", pool.getPoolName());
        messageRequest.addRequestData("adminFirstName", admin.getFirstName());
        messageRequest.addRequestData("adminLastName", admin.getLastName());

        mailService.sendEmail(messageRequest);
    }

    private List<String> getMessages(String message) {
        List<String> messages = Lists.newArrayList(message.split("(\r\n|\r|\n)"));

        // Remove any blanks or nulls from consecutive newlines. Consider leaving these for spacing?
        messages.removeIf(StringUtils::isBlank);

        return messages;
    }

    private void validateUserIsPoolAdmin(long userId, long poolId) {
        Set<UserPoolEntity> userPools = userPoolRepository.findByUserId(userId);
        if (CollectionUtils.isEmpty(userPools)) {
            throw new RuntimeException(String.format("No pools found for user with ID %d", userId));
        }

        Optional<UserPoolEntity> result =
            userPools.stream()
                .filter(entry -> entry.getPoolId().equals(poolId))
                .findFirst();

        if (!result.isPresent()) {
            throw new RuntimeException(String.format("User with ID %d is not associated with pool with ID %d", userId, poolId));
        }

        UserPoolEntity userPool = result.get();
        if (!UserPoolRole.ADMIN.name().equals(userPool.getUserRole())) {
            throw new RuntimeException(String.format("User with ID %d is not an admin of pool with ID %d", userId, poolId));
        }
    }
}
