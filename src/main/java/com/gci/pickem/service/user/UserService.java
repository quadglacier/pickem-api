package com.gci.pickem.service.user;

import com.gci.pickem.model.*;

import java.util.Collection;

public interface UserService {

    UserView getUserById(Long id);

    UserView getUserByUsername(String username);

    void grantPickViewForUsers(long grantingUserId, long poolId, Collection<Long> grantedUserIds);

    Collection<UserView> getUsersGrantingPickViewAccessForUserAndPool(long requestingUserId, long poolId);

    void createUser(UserCreationRequest user);

    void confirmUser(UserConfirmationView confirmationView);

    void changePassword(long userId, ChangePasswordRequest request);

    void userForgotPassword(ForgotPasswordRequest request);
}
