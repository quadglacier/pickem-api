package com.gci.pickem.service.user;

import com.gci.pickem.data.*;
import com.gci.pickem.exception.MissingRequiredDataException;
import com.gci.pickem.exception.UserAlreadyExistsException;
import com.gci.pickem.exception.notfound.PoolNotFoundException;
import com.gci.pickem.exception.notfound.UserNotFoundException;
import com.gci.pickem.model.*;
import com.gci.pickem.repository.*;
import com.gci.pickem.service.mail.MailService;
import com.gci.pickem.service.mail.MailType;
import com.gci.pickem.service.mail.SendEmailRequest;
import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;
    private PoolRepository poolRepository;
    private UserPoolRepository userPoolRepository;
    private PickViewGrantRepository pickViewGrantRepository;
    private PasswordEncoder passwordEncoder;
    private MailService mailService;

    private static final Comparator<UserView> USER_VIEW_COMPARATOR = (o1, o2) -> {
        if (o1.getLastName().equals(o2.getLastName())) {
            return o1.getFirstName().compareTo(o2.getFirstName());
        }

        return o1.getLastName().compareTo(o2.getLastName());
    };

    @Autowired
    UserServiceImpl(
        UserRepository userRepository,
        UserRoleRepository userRoleRepository,
        PoolRepository poolRepository,
        UserPoolRepository userPoolRepository,
        PickViewGrantRepository pickViewGrantRepository,
        PasswordEncoder passwordEncoder,
        MailService mailService
    ) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.poolRepository = poolRepository;
        this.userPoolRepository = userPoolRepository;
        this.pickViewGrantRepository = pickViewGrantRepository;
        this.passwordEncoder = passwordEncoder;
        this.mailService = mailService;
    }

    @Override
    public UserView getUserById(Long id) {
        UserEntity user = userRepository.findOne(id);
        if (user == null) {
            return null;
        }

        return new UserView(user);
    }

    @Override
    public UserView getUserByUsername(String username) {
        Optional<UserEntity> user = userRepository.findByEmail(username);
        return user.map(UserView::new).orElse(null);
    }

    @Override
    @Transactional
    public Collection<UserView> getUsersGrantingPickViewAccessForUserAndPool(long requestingUserId, long poolId) {
        // If the user is the admin of the pool, they have access to every user in the pool.
        UserEntity requestingUser = userRepository.findOne(requestingUserId);
        if (requestingUser == null) {
            throw new UserNotFoundException(requestingUserId);
        }

        UserPoolEntity adminUserPool =
            requestingUser.getUserPools()
                .stream()
                .filter(userPool -> userPool.getPoolId() == poolId)
                .filter(userPool -> UserPoolRole.ADMIN.name().equals(userPool.getUserRole()))
                .findFirst().orElse(null);
        if (adminUserPool != null) {
            // Requesting user is an admin of this pool, they have access to every other pool user's picks.
            return
                userPoolRepository.findByPoolId(adminUserPool.getPoolId())
                    .stream()
                    .filter(userPool -> userPool.getUserId() != requestingUserId) // Remove themselves
                    .map(UserPoolEntity::getUser)
                    .map(UserView::new)
                    .sorted(USER_VIEW_COMPARATOR)
                    .collect(Collectors.toList());
        }

        return
            pickViewGrantRepository.findByGrantedUserIdAndGrantedPoolId(requestingUserId, poolId)
                .stream()
                .map(pickViewGrant -> new UserView(pickViewGrant.getGrantingUser()))
                .sorted(USER_VIEW_COMPARATOR)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void grantPickViewForUsers(long grantingUserId, long poolId, Collection<Long> grantedUserIds) {
        PoolEntity pool = poolRepository.findOne(poolId);
        if (pool == null) {
            throw new PoolNotFoundException(poolId);
        }

        UserEntity requestingUser = userRepository.findOne(grantingUserId);
        if (requestingUser == null) {
            throw new UserNotFoundException(grantingUserId);
        }

        List<PickViewGrantEntity> grants =
            grantedUserIds.stream()
                .filter(userId -> userRepository.findOne(userId) != null)
                .map(userId -> {
                    PickViewGrantEntity grant = new PickViewGrantEntity();

                    grant.setGrantingUserId(grantingUserId);
                    grant.setGrantedPoolId(poolId);
                    grant.setGrantedUserId(userId);

                    return grant;
                })
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(grants)) {
            pickViewGrantRepository.save(grants);
        } else {
            log.info("No valid grants created.");
        }
    }

    @Override
    @Transactional
    public void createUser(UserCreationRequest request) {
        if (!userCreateRequestIsValid(request)) {
            String msg = "User registration requests require a first name, last name, and username";
            log.warn(msg);
            throw new MissingRequiredDataException(msg);
        }

        Optional<UserEntity> existingUser = userRepository.findByEmail(request.getEmail());
        if (existingUser.isPresent()) {
            String msg = String.format("User with email %s already exists.", request.getEmail());
            log.warn(msg);
            throw new UserAlreadyExistsException(msg);
        }

        // User data is present, no already existing user, let's create!
        UserEntity user = new UserEntity();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setEmail(request.getEmail());

        // Generate the nonce for the new user.
        String nonce = generateNonce();
        user.setRegistrationCode(nonce);

        user = userRepository.save(user);

        // Assign the user the USER role!
        UserRoleEntity role = new UserRoleEntity();
        role.setUserId(user.getUserId());
        role.setRole("USER");

        role = userRoleRepository.save(role);

        user.setUserRoles(Sets.newHashSet(role));

        // New user was created successfully. Send an email so the user can set the password.
        sendUserNonceEmail(user, nonce, MailType.USER_REGISTRATION, request.getClientUrl());
    }

    private String generateNonce() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }

    @Override
    @Transactional
    public void userForgotPassword(ForgotPasswordRequest request) {
        Optional<UserEntity> result = userRepository.findByEmail(request.getUserEmail());
        if (!result.isPresent()) {
            String msg = String.format("No user found with email %s", request.getUserEmail());
            log.warn(msg);
            throw new UserNotFoundException(msg);
        }

        // Update the user's nonce and send the forgot password email.
        UserEntity user = result.get();

        String nonce = generateNonce();

        user.setRegistrationCode(nonce);
        user = userRepository.save(user);

        sendUserNonceEmail(user, nonce, MailType.FORGOT_PASSWORD, request.getClientUrl());
    }

    private void sendUserNonceEmail(UserEntity user, String nonce, MailType mailType, String clientUrl) {
        SendEmailRequest request = new SendEmailRequest();
        request.setTemplateId(mailType.getTemplateId());
        request.setRecipientEmail(user.getEmail());
        request.setRecipientName(user.getFirstName());

        request.addRequestData("nonce", nonce);
        request.addRequestData("firstName", user.getFirstName());
        request.addRequestData("clientUrl", clientUrl);

        mailService.sendEmail(request);
    }

    @Override
    public void changePassword(long userId, ChangePasswordRequest request) {
        UserEntity user = userRepository.findOne(userId);

        if (!passwordEncoder.matches(request.getCurrentPassword(), user.getPassword())) {
            // User provided an incorrect current password. Can't allow a change!
            throw new RuntimeException("Current password is incorrect.");
        }

        if (request.getCurrentPassword().equals(request.getNewPassword())) {
            throw new RuntimeException("New and current password are identical.");
        }

        // Everything is in order, allow the new password!
        user.setPassword(passwordEncoder.encode(request.getNewPassword()));

        userRepository.save(user);
    }

    @Override
    public void confirmUser(UserConfirmationView confirmationView) {
        Optional<UserEntity> result = userRepository.findByRegistrationCode(confirmationView.getNonce());
        if (!result.isPresent()) {
            String msg = "No user found for the provided registration code.";
            log.warn(msg);
            throw new UserNotFoundException(msg);
        }

        UserEntity user = result.get();

        user.setPassword(passwordEncoder.encode(confirmationView.getPassword()));

        userRepository.save(user);
    }

    private boolean userCreateRequestIsValid(UserCreationRequest user) {
        return
            StringUtils.isNotBlank(user.getFirstName()) &&
            StringUtils.isNotBlank(user.getLastName()) &&
            StringUtils.isNotBlank(user.getEmail());
    }
}
