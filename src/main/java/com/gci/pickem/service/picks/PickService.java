package com.gci.pickem.service.picks;

import com.gci.pickem.model.GamePick;
import com.gci.pickem.model.PickConfidenceData;
import com.gci.pickem.model.PickDataResponse;
import com.gci.pickem.model.UserPicksRequest;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface PickService {

    PickSubmissionResponse saveUserPicks(long userId, UserPicksRequest request);

    List<GamePick> getUserPicks(long userId, long poolId, int season, int week);

    List<GamePick> getUserPicks(long requestingUserId, long userId, long poolId, int season, int week);

    List<Integer> getConfidenceValues(long poolId, int season, int week);

    PickDataResponse getPickData(long poolId, int season, int week);

    Map<String, Object> getDataForTeams(int season);
    Map<Integer, PickConfidenceData> getDataForConfidences(int season);

    void sendUserPicksEmail(long userId, long poolId, int season, int week);

    /**
     * Send out an email notification to all users who don't have all of their picks
     * made for all of the games on the schedule within the next 24 hours following
     * the date provided.
     *
     * This will send an email to users missing picks for each of the pools they are
     * missing picks for.
     *
     * For example, if a user is in three pools (1, 2, and 3) and there are two games
     * in the next 24 hours and they have picks for both games in pool 1, one of the
     * games in pool 2, and neither of the two games in pool 3, the user will be sent
     * two emails: one for pool 2 and one for pool 3. Both emails will list only the
     * games that the user is missing picks for.
     *
     * @param date the date indicating the start of the 24-hour window being used to
     *             check for missing user picks.
     */
    void notifyUsersWithoutPicks(LocalDate date);
}
