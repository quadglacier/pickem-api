package com.gci.pickem.service.picks;

import com.gci.pickem.data.GameEntity;
import com.gci.pickem.data.*;
import com.gci.pickem.exception.InvalidScoringMethodException;
import com.gci.pickem.exception.MissingRequiredDataException;
import com.gci.pickem.exception.notfound.PoolNotFoundException;
import com.gci.pickem.exception.notfound.UserNotFoundException;
import com.gci.pickem.model.*;
import com.gci.pickem.repository.*;
import com.gci.pickem.service.mail.MailService;
import com.gci.pickem.service.mail.MailType;
import com.gci.pickem.service.mail.SendEmailRequest;
import com.gci.pickem.service.schedule.ScheduleService;
import com.gci.pickem.util.PickEmConstants;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PickServiceImpl implements PickService {
    private static final Logger log = LoggerFactory.getLogger(PickServiceImpl.class);

    private PickRepository pickRepository;
    private GameRepository gameRepository;
    private PoolRepository poolRepository;
    private UserRepository userRepository;
    private PickViewGrantRepository pickViewGrantRepository;
    private ScheduleService scheduleService;
    private MailService mailService;

    @Autowired
    PickServiceImpl(
        PickRepository pickRepository,
        GameRepository gameRepository,
        PoolRepository poolRepository,
        UserRepository userRepository,
        PickViewGrantRepository pickViewGrantRepository,
        ScheduleService scheduleService,
        MailService mailService
    ) {
        this.pickRepository = pickRepository;
        this.gameRepository = gameRepository;
        this.poolRepository = poolRepository;
        this.userRepository = userRepository;
        this.pickViewGrantRepository = pickViewGrantRepository;
        this.scheduleService = scheduleService;
        this.mailService = mailService;
    }

    @Override
    public PickSubmissionResponse saveUserPicks(long userId, UserPicksRequest request) {
        if (request == null || request.getPoolId() == null ||
            CollectionUtils.isEmpty(request.getGamePicks())) {
            String msg = "Insufficient information received to record picks for user.";
            log.info(msg);
            throw new MissingRequiredDataException(msg);
        }

        List<GamePick> picks = request.getGamePicks();

        validateUserValidForPool(userId, request.getPoolId());
        Map<Long, PickInvalidityReason> validityResults = validateGames(userId, request.getPoolId(), picks);
        if (!validityResults.isEmpty()) {
            // There are some invalid picks in the submission.
            return new PickSubmissionResponse(validityResults);
        }

        validatePicksValidForPool(request.getPoolId(), picks);

        // Everything appears to be valid. Let's save them picks!
        int season = getSeason(picks);
        int week = getWeek(picks);

        Set<PickEntity> existingPicks = pickRepository.getPicks(userId, request.getPoolId(), season, week);
        if (CollectionUtils.isEmpty(existingPicks)) {
            // User has not saved any picks for this season/week/pool yet. Make all new ones.
            List<PickEntity> toCreate =
                request.getGamePicks().stream()
                    .map(gamePick -> {
                        PickEntity pick = new PickEntity();

                        pick.setUserId(userId);
                        pick.setPoolId(request.getPoolId());
                        pick.setGameId(gamePick.getGameId());
                        pick.setConfidence(gamePick.getConfidence());
                        pick.setChosenTeamId(gamePick.getChosenTeamId());

                        return pick;
                    })
                    .collect(Collectors.toList());

            pickRepository.save(toCreate);
        } else {
            // User has existing picks saved, update confidences and teams.
            Map<Long, GamePick> picksByGameId = new HashMap<>();
            picks.forEach(pick -> picksByGameId.put(pick.getGameId(), pick));

            existingPicks.forEach(pick -> {
                GamePick gamePick = picksByGameId.get(pick.getGameId());
                pick.setConfidence(gamePick.getConfidence());
                pick.setChosenTeamId(gamePick.getChosenTeamId());
            });

            List<PickEntity> toSave = Lists.newArrayList(existingPicks);

            // Get the list of games missing picks.
            Set<Long> missingGameIds =
                picksByGameId.keySet()
                    .stream()
                    .filter(gameId ->
                        !existingPicks.stream()
                            .map(PickEntity::getGameId)
                            .collect(Collectors.toSet()).contains(gameId))
                    .collect(Collectors.toSet());

            if (!missingGameIds.isEmpty()) {
                missingGameIds.forEach(gameId -> {
                    GamePick gamePick = picksByGameId.get(gameId);

                    PickEntity pick = new PickEntity();

                    pick.setGameId(gameId);
                    pick.setUserId(userId);
                    pick.setPoolId(request.getPoolId());
                    pick.setChosenTeamId(gamePick.getChosenTeamId());
                    pick.setConfidence(gamePick.getConfidence());

                    toSave.add(pick);
                });
            }

            pickRepository.save(toSave);
        }

        // Success response.
        return new PickSubmissionResponse();
    }

    @Override
    public List<GamePick> getUserPicks(long requestingUserId, long userId, long poolId, int season, int week) {
        if (requestingUserId == userId) {
            // User is requesting their own picks, just call the method that does that.
            return getUserPicks(userId, poolId, season, week);
        }

        // User is requesting picks of another user. Only allowable if the requesting user is pool admin.
        UserEntity requester = userRepository.findOne(requestingUserId);
        if  (requester == null) {
            throw new UserNotFoundException(requestingUserId);
        }

        PickViewGrantEntity grant =
            pickViewGrantRepository.findByGrantedUserId(requestingUserId)
                .stream()
                .filter(pickViewGrant -> pickViewGrant.getGrantedPoolId() == poolId)
                .filter(pickViewGrant -> pickViewGrant.getGrantingUserId() == userId)
                .findFirst()
                .orElse(null);

        if (grant == null) {
            // User has not been granted access to see the specified user's picks for this pool.
            // They will have to be the pool admin in order to otherwise see the picks for this user.
            PoolEntity pool =
                requester.getUserPools()
                    .stream()
                    .filter(userPool -> UserPoolRole.ADMIN.name().equals(userPool.getUserRole()))
                    .map(UserPoolEntity::getPool)
                    .filter(userPool -> userPool.getPoolId() == poolId)
                    .findFirst()
                    .orElse(null);

            if (pool == null) {
                throw new AccessDeniedException("Requesting user is not permitted to see the picks of the specified user for this pool.");
            }
        }

        // User is either an admin of the specified pool or has been granted access to see this users's picks.
        return getUserPicks(userId, poolId, season, week);
    }

    @Override
    public List<GamePick> getUserPicks(long userId, long poolId, int season, int week) {
        Set<PickEntity> picks = pickRepository.getPicks(userId, poolId, season, week);
        if (CollectionUtils.isEmpty(picks)) {
            return new ArrayList<>();
        }

        return
            picks.stream()
                .map(pick -> {
                    GamePick gamePick = new GamePick();

                    gamePick.setGameId(pick.getGameId());
                    gamePick.setConfidence(pick.getConfidence());
                    gamePick.setChosenTeamId(pick.getChosenTeamId());

                    return gamePick;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> getDataForTeams(int season) {
        return null;
    }

    @Override
    public Map<Integer, PickConfidenceData> getDataForConfidences(int season) {
        Map<Integer, PickConfidenceData> dataByConfidence = new HashMap<>();

        for (int i = 1; i <= 16; i++) {
            PickConfidenceData data = new PickConfidenceData();

            Integer picks = pickRepository.getTotalPicksForConfidenceAndSeason(i, season);
            data.setTotalPicks(picks != null ? picks : 0);

            Integer correct = pickRepository.getCorrectPicksForConfidenceAndSeason(i, season);
            data.setCorrectPicks(correct != null ? correct : 0);

            if (data.getTotalPicks() > 0) {
                BigDecimal accuracy = BigDecimal.valueOf((1.0d * data.getCorrectPicks()) / data.getTotalPicks());
                data.setAccuracy(accuracy.multiply(BigDecimal.valueOf(100)));
            } else {
                data.setAccuracy(BigDecimal.ZERO);
            }

            dataByConfidence.put(i, data);
        }

        return dataByConfidence;
    }

    @Override
    public PickDataResponse getPickData(long poolId, int season, int week) {
        PickDataResponse response = new PickDataResponse();

        Set<PickEntity> picks = pickRepository.getPicks(poolId, season, week);

        // Get the games for this season/week
        List<GameEntity> games = gameRepository.findAllBySeasonAndWeek(season, week);

        Map<Long, PickData> pickDatabyGame = new HashMap<>();

        // For each game get the number of picks for each team and the average confidence for each team.
        Instant now = Instant.now();
        games.forEach(game -> {
            PickData pickData = new PickData();
            if (game.getGameTimeEpoch() == null || !now.isAfter(Instant.ofEpochMilli(game.getGameTimeEpoch()))) {
                pickData.setMessage("Game has not started yet. Unable to view pick data.");
            } else {
                TeamEntity home = game.getHomeTeam();
                TeamEntity away = game.getAwayTeam();

                AtomicInteger homePicks = new AtomicInteger();
                AtomicInteger awayPicks = new AtomicInteger();
                AtomicInteger totalPicks = new AtomicInteger();
                AtomicInteger homeConfidence = new AtomicInteger();
                AtomicInteger awayConfidence = new AtomicInteger();
                picks.stream()
                    .filter(pick -> pick.getGameId().equals(game.getGameId()))
                    .filter(pick -> pick.getConfidence() != null && pick.getChosenTeamId() != null)
                    .forEach(pick -> {
                        totalPicks.getAndIncrement();
                        if (pick.getChosenTeamId().equals(home.getTeamId())) {
                            homePicks.getAndIncrement();
                            homeConfidence.addAndGet(pick.getConfidence());
                        } else if (pick.getChosenTeamId().equals(away.getTeamId())) {
                            awayPicks.getAndIncrement();
                            awayConfidence.addAndGet(pick.getConfidence());
                        }
                    });


                // Processed all the picks, add the data to the response.
                pickData.setHomeTeam(home.getAbbreviation());
                pickData.setHomePickPct(
                    BigDecimal.valueOf((homePicks.get() * 100.0) / totalPicks.get()).setScale(2, RoundingMode.HALF_UP));
                if (homePicks.get() != 0) {
                    pickData.setHomeAvgConfidence(
                        BigDecimal.valueOf(homeConfidence.doubleValue() / homePicks.get()).setScale(2, RoundingMode.HALF_UP));
                } else {
                    pickData.setHomeAvgConfidence(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
                }

                pickData.setAwayTeam(away.getAbbreviation());
                pickData.setAwayPickPct(
                    BigDecimal.valueOf((awayPicks.get() * 100.0) / totalPicks.get()).setScale(2, RoundingMode.HALF_UP));
                if (awayPicks.get() != 0) {
                    pickData.setAwayAvgConfidence(
                        BigDecimal.valueOf(awayConfidence.doubleValue() / awayPicks.get()).setScale(2, RoundingMode.HALF_UP));
                } else {
                    pickData.setAwayAvgConfidence(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
                }
            }

            pickDatabyGame.put(game.getGameId(), pickData);
        });

        response.setSeason(season);
        response.setWeek(week);
        response.setPoolId(poolId);
        response.setDataByGame(pickDatabyGame);

        return response;
    }

    @Override
    public List<Integer> getConfidenceValues(long poolId, int season, int week) {
        PoolEntity pool = poolRepository.findOne(poolId);
        if (pool == null) {
            throw new PoolNotFoundException(poolId);
        }

        ScoringMethod method = ScoringMethod.getScoringMethodById(pool.getScoringMethod());
        if (method == null) {
            throw new InvalidScoringMethodException(String.format("No scoring method found for pool with ID %d", poolId));
        }

        GamesList gamesList = scheduleService.getGamesForSeasonAndWeek(season, week);

        switch (method) {
            case ABSOLUTE:
                return Collections.nCopies(gamesList.getGames().size(), 1);
            case SIXTEEN_DOWN:
                List<Integer> values = new ArrayList<>();

                int nextVal = 16;
                for (Iterator<com.gci.pickem.model.Game> iter = gamesList.getGames().iterator(); iter.hasNext(); iter.next()) {
                    values.add(nextVal--);
                }

                return values;
            default:
                throw new InvalidScoringMethodException(String.format("Unexpected method %s found for pool with ID %d", method, poolId));
        }
    }

    @Override
    @Transactional
    public void notifyUsersWithoutPicks(LocalDate today) {
        if (today == null) {
            today = LocalDate.now(ZoneId.of(PickEmConstants.MSF_TIMEZONE));
        }

        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);

        log.info("Finding games between {} and {}", todayMidnight, tomorrowMidnight);

        // Find all games
        long todayMilli = todayMidnight.atZone(ZoneId.of(PickEmConstants.MSF_TIMEZONE)).toInstant().toEpochMilli();
        long tomorrowMilli = tomorrowMidnight.atZone(ZoneId.of(PickEmConstants.MSF_TIMEZONE)).toInstant().toEpochMilli();
        List<GameEntity> games =
            gameRepository.findByGameTimeEpochBetween(
                todayMilli,
                tomorrowMilli);

        if (games.isEmpty()) {
            // There are no games in this 24-hour period, no alerts need to be sent out.
            log.info("No games found between {} and {}", todayMilli, tomorrowMilli);
            return;
        }

        log.info("Looking for users who are missing any picks for the {} games today.", games.size());

        List<Long> gameIds = games.stream().map(GameEntity::getGameId).collect(Collectors.toList());

        log.info("Game IDs for today's games: {}", gameIds);

        // Find all users in pools who haven't made picks for the given games
        Set<Object[]> usersMissingPicks = userRepository.getUsersWithMissingPicks(gameIds, gameIds.size());

        log.info("Found {} user/pool combinations missing picks for today's games", usersMissingPicks.size());

        // This could get expensive later. Watch out.
        Map<Long, PoolEntity> poolCache = new HashMap<>();

        List<SendEmailRequest> requests = new ArrayList<>();
        for (Object[] result : usersMissingPicks) {
            try {
                Long userId = Long.valueOf((Integer) result[0]);
                Long poolId = Long.valueOf((Integer) result[1]);

                UserEntity user = userRepository.findOne(userId);
                validateUserValidForPool(user.getUserId(), poolId);

                // User belongs to pool.
                PoolEntity pool = getPool(poolId, poolCache);

                SendEmailRequest request = new SendEmailRequest();
                request.setRecipientName(user.getFirstName());
                request.setRecipientEmail(user.getEmail());
                request.setTemplateId(MailType.PICKS_REMINDER.getTemplateId());

                request.addRequestData("poolName", pool.getPoolName());
                request.addRequestData("firstName", user.getFirstName());
                request.addRequestData("clientUrl", pool.getClientUrl());

                GameEntity game = games.get(0);
                log.info(
                    "Looking up user {}'s picks for pool {}, week {}, season {}.",
                    userId,
                    poolId,
                    game.getWeek(),
                    game.getSeason());

                Set<PickEntity> picks = pickRepository.getPicks(userId, poolId, game.getSeason(), game.getWeek());

                log.info(
                    "Found {} existing picks: {}",
                    picks.size(),
                    picks.stream().map(PickEntity::getGameId).collect(Collectors.toSet()));

                // Start by assuming they're missing everything.
                Set<Long> missingPickGameIds = new HashSet<>(gameIds);
                if (!picks.isEmpty()) {
                    // Remove any games that the user has a saved pick for.
                    missingPickGameIds.removeAll(
                        picks.stream()
                            .filter(pick -> gameIds.contains(pick.getGameId()) && pick.getConfidence() != null)
                            .map(PickEntity::getGameId)
                            .collect(Collectors.toList()));
                }

                Iterable<GameEntity> gamesNeedingPicks = gameRepository.findAll(missingPickGameIds);

                log.info(
                    "Games needing picks: {}",
                    StreamSupport.stream(gamesNeedingPicks.spliterator(), false)
                        .map(GameEntity::getGameId)
                        .collect(Collectors.toSet()));

                List<Map<String, String>> missingGames = new ArrayList<>();
                for (GameEntity needingPick : gamesNeedingPicks) {
                    Map<String, String> missingGame = new HashMap<>();

                    missingGame.put("awayTeamName", needingPick.getAwayTeam().getTeamName());
                    missingGame.put("homeTeamName", needingPick.getHomeTeam().getTeamName());
                    missingGame.put("gameTime", Instant.ofEpochMilli(needingPick.getGameTimeEpoch()).toString());

                    missingGames.add(missingGame);
                }

                request.addRequestData("missingGames", missingGames);

                requests.add(request);
            } catch (Exception e) {
                log.error("", e);
            }
        }

        mailService.sendEmails(requests);
    }

    @Override
    public void sendUserPicksEmail(long userId, long poolId, int season, int week) {
        PoolEntity pool = poolRepository.findOne(poolId);
        if (pool == null) {
            throw new PoolNotFoundException(poolId);
        }

        UserEntity user = userRepository.findOne(userId);
        if (user == null) {
            throw new UserNotFoundException(userId);
        }

        List<GameEntity> allBySeasonAndWeek = gameRepository.findAllBySeasonAndWeek(season, week);

        Set<PickEntity> picks = pickRepository.getPicks(userId, poolId, season, week);

        List<Map<String, Object>> pickData = new ArrayList<>();
        for (GameEntity game : allBySeasonAndWeek) {
            Optional<PickEntity> match = picks.stream().filter(p -> p.getGameId().equals(game.getGameId())).findFirst();

            TeamEntity chosenTeam = null;
            Integer confidence = null;
            if (match.isPresent()) {
                PickEntity pick = match.get();
                if (pick.getChosenTeamId() != null) {
                    chosenTeam =
                        pick.getChosenTeamId().equals(game.getHomeTeamId()) ?
                            game.getHomeTeam() :
                            game.getAwayTeam();
                }

                confidence = pick.getConfidence();
            }

            Map<String, Object> data = new HashMap<>();
            data.put("homeTeam", game.getHomeTeam().getTeamName());
            data.put("awayTeam", game.getAwayTeam().getTeamName());
            data.put("chosenTeam", chosenTeam != null ? chosenTeam.getTeamName() : null);
            data.put("confidence", confidence);

            pickData.add(data);
        }

        SendEmailRequest request = new SendEmailRequest();
        request.setTemplateId(MailType.MY_WEEK_PICKS.getTemplateId());
        request.setRecipientName(user.getFirstName());
        request.setRecipientEmail(user.getEmail());

        request.addRequestData("picks", pickData);
        request.addRequestData("clientUrl", pool.getClientUrl());
        request.addRequestData("week", week);
        request.addRequestData("season", season);
        request.addRequestData("poolName", pool.getPoolName());

        mailService.sendEmail(request);
    }

    private PoolEntity getPool(Long poolId, Map<Long, PoolEntity> cache) {
        PoolEntity pool = cache.get(poolId);
        if (pool != null) {
            return pool;
        }

        pool = poolRepository.findOne(poolId);
        if (pool == null) {
            throw new PoolNotFoundException(poolId);
        }

        cache.put(poolId, pool);

        return pool;
    }

    private int getWeek(List<GamePick> picks) {
        Set<Long> gameIds = picks.stream().map(GamePick::getGameId).collect(Collectors.toSet());

        Iterable<GameEntity> allGames = gameRepository.findAll(gameIds);

        Set<Integer> weeks = StreamSupport.stream(allGames.spliterator(), false).map(GameEntity::getWeek).collect(Collectors.toSet());
        if (weeks.size() != 1) {
            // Every game in the submission should have the same single week value.
            throw new RuntimeException(String.format("Expected only one week for the games provided in picks, found %d instead.", weeks.size()));
        }

        return weeks.iterator().next();
    }

    private int getSeason(List<GamePick> picks) {
        Set<Long> gameIds = picks.stream().map(GamePick::getGameId).collect(Collectors.toSet());

        Iterable<GameEntity> allGames = gameRepository.findAll(gameIds);

        Set<Integer> seasons = StreamSupport.stream(allGames.spliterator(), false).map(GameEntity::getSeason).collect(Collectors.toSet());
        if (seasons.size() != 1) {
            // Every game in the submission should have the same single season value.
            throw new RuntimeException(String.format("Expected only one season for the games provided in picks, found %d instead.", seasons.size()));
        }

        return seasons.iterator().next();
    }

    private void validatePicksValidForPool(Long poolId, List<GamePick> picks) {
        if (poolId == null) {
            throw new MissingRequiredDataException("No pool ID provided for pick submission request.");
        }

        if (CollectionUtils.isEmpty(picks)) {
            throw new MissingRequiredDataException("No picks provided for pick submission request.");
        }

        PoolEntity pool = poolRepository.findOne(poolId);

        ScoringMethod method = ScoringMethod.getScoringMethodById(pool.getScoringMethod());
        if (method == null) {
            throw new InvalidScoringMethodException(String.format("No scoring method found for pool with ID %d", poolId));
        }

        List<Integer> confidences = picks.stream().map(GamePick::getConfidence).collect(Collectors.toList());
        if (!method.areConfidencesValid(confidences)) {
            throw new RuntimeException(String.format("Invalid confidences provided for picks with pool using scoring method %s", method.getName()));
        }
    }

    void validateUserValidForPool(Long userId, Long poolId) {
        if (userId == null) {
            throw new RuntimeException("No user ID provided for pick submission request.");
        }

        if (poolId == null) {
            throw new RuntimeException("No pool ID provided for pick submission request.");
        }

        UserEntity user = userRepository.findOne(userId);

        if (user == null) {
            throw new RuntimeException(String.format("User with ID %d not found.", userId));
        }

        if (CollectionUtils.isEmpty(user.getUserPools())) {
            throw new RuntimeException(String.format("User with ID %d doesn't belong to any pools.", userId));
        }

        Set<Long> poolIds = user.getUserPools().stream().map(UserPoolEntity::getPoolId).collect(Collectors.toSet());

        if (!poolIds.contains(poolId)) {
            throw new RuntimeException(String.format("User with ID %d does not belong to pool with ID %d", userId, poolId));
        }
    }

    /**
     * Ensure that the correct number of games are provided in the picks and that all picks
     * span just one week.
     */
    Map<Long, PickInvalidityReason> validateGames(long userId, long poolId, List<GamePick> picks) {
        PoolEntity pool = poolRepository.findOne(poolId);
        if (pool == null) {
            throw new PoolNotFoundException(poolId);
        }

        Set<Long> gameIds = picks.stream().map(GamePick::getGameId).collect(Collectors.toSet());

        if (gameIds.size() != picks.size()) {
            // We apparently doubled up on a game somehow.
            throw new RuntimeException(String.format("Game picks provided should have covered %d games, but only covered %d.", picks.size(), gameIds.size()));
        }

        int week = getWeek(picks);
        int season = getSeason(picks);

        List<GameEntity> gamesForWeek = gameRepository.findAllBySeasonAndWeek(season, week);
        if (gamesForWeek.size() != picks.size()) {
            // Always expect a number of picks objects equal to the number of games in the week, even if some confidences are left empty for now.
            throw new RuntimeException(String.format("Unexpected number of picks provided. Expected %d, but received %d", gamesForWeek.size(), picks.size()));
        }

        // Validate that their picks match the games (can't pick the Vikings to win a game between the Chiefs and Steelers!)
        Map<Long, GameEntity> gamesMap = new HashMap<>();
        for (GameEntity game : gamesForWeek) {
            gamesMap.put(game.getGameId(), game);
        }

        Map<Long, PickInvalidityReason> invalidityReasons = new HashMap<>();
        for (GamePick gamePick : picks) {
            if (gamePick.getChosenTeamId() == null) {
                // User hasn't specified a pick for this game. That's fine.
                continue;
            }

            GameEntity theGame = gamesMap.get(gamePick.getGameId());
            if (theGame == null) {
                log.warn("Received pick for game ID {} which isn't valid for game with week ID {}", gamePick.getGameId(), week);
                invalidityReasons.put(gamePick.getGameId(), PickInvalidityReason.GAME_NOT_FOUND);
                continue;
            }

            if (!gamePick.getChosenTeamId().equals(theGame.getHomeTeamId()) && !gamePick.getChosenTeamId().equals(theGame.getAwayTeamId())) {
                log.warn("User has selected team with ID {} in a game between teams with IDs {} and {}.", gamePick.getChosenTeamId(), theGame.getAwayTeamId(), theGame.getHomeTeamId());
                invalidityReasons.put(gamePick.getGameId(), PickInvalidityReason.INVALID_CHOSEN_TEAM);
            }

            Instant now = Instant.now();
            // Adjust the game time by the grade period of the pool to allow for slightly late picks.
            Instant gameTime = Instant.ofEpochMilli(theGame.getGameTimeEpoch() + pool.getPickGracePeriod());
            if (gameTime.isBefore(now)) {
                PickEntity existingPick = pickRepository.getPickByUserIdAndPoolIdAndGameId(userId, poolId, theGame.getGameId());
                if (!isPickValidForGameInProgress(existingPick, gamePick)) {
                    log.warn("Invalid submission of pick or change to existing pick made for in-progress game with ID {}", gamePick.getGameId());
                    invalidityReasons.put(gamePick.getGameId(), PickInvalidityReason.GAME_STARTED);
                }
            }
        }

        return invalidityReasons;
    }

    private boolean isPickValidForGameInProgress(PickEntity existingPick, GamePick incomingPick) {
        if (existingPick == null) {
            return incomingPick.getChosenTeamId() == null && incomingPick.getConfidence() == null;
        } else {
            // There is an existing pick. The confidences and teams better match!
            if ((existingPick.getChosenTeamId() == null) != (incomingPick.getChosenTeamId() == null)) {
                // One is null, but the other isn't. Doesn't matter which is which, this isn't allowed.
                return false;
            } else if (existingPick.getChosenTeamId() != null) {
                if (!existingPick.getChosenTeamId().equals(incomingPick.getChosenTeamId())) {
                    // Both values are non-null, but their values don't match. Not okay.
                    return false;
                }
            }

            if ((existingPick.getConfidence() == null) != (incomingPick.getConfidence() == null)) {
                // One is null, but the other isn't. Doesn't matter which is which, this isn't allowed.
                return false;
            } else if (existingPick.getConfidence() != null) {
                return existingPick.getConfidence().equals(incomingPick.getConfidence());
            }
        }

        return true;
    }
}
