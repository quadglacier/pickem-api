package com.gci.pickem.service.team;

import com.gci.pickem.data.TeamEntity;

public interface TeamService {

    void upsertTeam(TeamEntity team);

    TeamEntity findByExternalId(Long externalId);

    TeamEntity findById(Long teamId);
}
