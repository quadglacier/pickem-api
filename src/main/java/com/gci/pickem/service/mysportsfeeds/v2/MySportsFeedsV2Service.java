package com.gci.pickem.service.mysportsfeeds.v2;

import com.gci.pickem.model.mysportsfeeds.v2.GamesResponse;

import java.time.Instant;

public interface MySportsFeedsV2Service {

    GamesResponse getGamesForSeasonAndWeek(int season, int week);

    GamesResponse getGamesUntilDaysFromNow(int days);

    GamesResponse getFinalGamesForDate(Instant date);
}
