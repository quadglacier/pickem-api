package com.gci.pickem.service.mysportsfeeds.v2;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gci.pickem.exception.InvalidResponseReceivedException;
import com.gci.pickem.model.mysportsfeeds.v2.GamesResponse;
import com.gci.pickem.service.mysportsfeeds.MySportsFeedEndpoint;
import com.gci.pickem.util.PickEmConstants;
import com.gci.pickem.util.ScheduleUtil;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.ParametersAreNonnullByDefault;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class MySportsFeedsV2ServiceImpl implements MySportsFeedsV2Service {
    private static final Logger log = LoggerFactory.getLogger(MySportsFeedsV2ServiceImpl.class);

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final ObjectMapper MAPPER;

    private final CacheLoader<String, String> cacheLoader = new CacheLoader<>() {
        @Override
        @ParametersAreNonnullByDefault
        public String load(String key) {
            return getRawResponse(key);
        }

        private String getRawResponse(String requestUrl) {
            try {
                URL url = new URL(requestUrl);
                String encoding = Base64.getEncoder().encodeToString(String.format("%s:%s", username, password).getBytes());

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setRequestProperty("Authorization", "Basic " + encoding);
                connection.setRequestProperty("Cache-Control", "no-cache");

                return IOUtils.toString(connection.getInputStream(), StandardCharsets.UTF_8);
            } catch (Exception e) {
                log.trace("", e);
                log.error(String.format("Unable to get successful response for request URL %s: %s", requestUrl, e.getMessage()));
            }

            return null;
        }
    };

    private final LoadingCache<String, String> requestCache =
        CacheBuilder
            .newBuilder()
            .expireAfterAccess(1, TimeUnit.MINUTES)
            .build(cacheLoader);

    private final String username;
    private final String password;
    private final String dataFormat;
    private final String apiVersion;
    private final String baseUrl;

    @Autowired
    public MySportsFeedsV2ServiceImpl(
        @Value("${mysportsfeeds.v2.username}") String username,
        @Value("${mysportsfeeds.v2.password}") String password,
        @Value("${mysportsfeeds.v2.url.base}") String baseUrl,
        @Value("${mysportsfeeds.v2.version.current}") String apiVersion,
        @Value("${mysportsfeeds.v2.format}") String dataFormat
    ) {
        this.username = username;
        this.password = password;
        this.baseUrl = baseUrl;
        this.apiVersion = apiVersion;
        this.dataFormat = dataFormat;
    }

    static {
        MAPPER = new ObjectMapper();
        MAPPER.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        MAPPER.registerModule(new JavaTimeModule());
    }

    @Override
    public GamesResponse getGamesForSeasonAndWeek(int season, int week) {
        GamesResponse response = getGamesResponse(String.format("%s?week=%d", getScheduleUrl(season), week));

        if (response == null || CollectionUtils.isEmpty(response.getGames())) {
            throw new InvalidResponseReceivedException(String.format("No response retrieved for games request for season %d and week %d", season, week));
        }

        // Pull out anything from the games that aren't for the week in question.
        response.getGames().removeIf(game -> game.getSchedule() == null || game.getSchedule().getWeek() != week);

        return response;
    }

    @Override
    public GamesResponse getGamesUntilDaysFromNow(int days) {
        int season = ScheduleUtil.getSeasonForDate(Instant.now());
        GamesResponse response =
            getGamesResponse(
                String.format("%s?date=until-%d-days-from-now", getScheduleUrl(season), days));

        if (response == null) {
            throw new InvalidResponseReceivedException(
                String.format("No response retrieved for full game schedule request for season %d", season));
        }

        return response;
    }

    @Override
    public GamesResponse getFinalGamesForDate(Instant date) {
        String dateStr = getDateString(date);
        GamesResponse response =
            getGamesResponse(
                String.format(
                    "%s?date=%s&status=final",
                    getScheduleUrl(ScheduleUtil.getSeasonForDate(date)),
                    getDateString(date)));

        if (response == null) {
            throw new InvalidResponseReceivedException(
                    String.format("No response retrieved for scoreboard request for date %s", dateStr));
        }

        return response;
    }

    private String getScheduleUrl(int season) {
        return String.format("%s/%s/pull/nfl/%s/%s.%s", baseUrl, apiVersion, getRequestYear(season), MySportsFeedEndpoint.GAMES.getValue(), dataFormat);
    }

    private String getRequestYear(int season) {
        // If they send 2018, assume they meant 2018-2019 season. Anything past the current year, default to current.
        return
            Calendar.getInstance().get(Calendar.YEAR) < season ?
                "current" :
                String.format("%d-%d-regular", season, season + 1);
    }

    private GamesResponse getGamesResponse(String url) {
        try {
            log.debug("Executing request to {}", url);
            return MAPPER.readValue(requestCache.get(url), GamesResponse.class);
        } catch (ExecutionException | IOException e) {
            log.trace("", e);
            log.warn(String.format("Exception occurred while attempting to get scoreboard response: %s", e.getMessage()));
        }

        return null;
    }

    private String getDateString(Instant date) {
        return DATE_TIME_FORMATTER.format(date.atZone(ZoneId.of(PickEmConstants.MSF_TIMEZONE)));
    }
}
