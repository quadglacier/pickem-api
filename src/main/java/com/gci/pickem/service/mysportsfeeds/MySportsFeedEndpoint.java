package com.gci.pickem.service.mysportsfeeds;

public enum MySportsFeedEndpoint {
    GAMES("games");

    private final String value;

    MySportsFeedEndpoint(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
