package com.gci.pickem.service.mail;

public enum MailType {
    PICKS_REMINDER("tem_hqtMmttPpSQJTg668PHkD4dP"),
    USER_REGISTRATION("tem_tbk79DqGrvWpfrM7JQTT8RP8"),
    FORGOT_PASSWORD("tem_Gcp6cRPpYcTHrkS4jj3D8XVT"),
    ADMIN_POOL_MESSAGE("tem_H8GtFpxFBpF3b7BDbR7qd8RH"),
    POOL_INVITE("tem_jbHYWHKyJQmC3qbXpVyRkH3B"),
    MY_WEEK_PICKS("tem_PXyQGPGydJXCYwWRj9HKKxcD");

    private final String templateId;

    MailType(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateId() {
        return templateId;
    }
}
