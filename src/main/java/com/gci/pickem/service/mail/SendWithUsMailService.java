package com.gci.pickem.service.mail;

import com.google.common.collect.ImmutableMap;
import com.sendwithus.SendWithUs;
import com.sendwithus.SendWithUsSendRequest;
import com.sendwithus.exception.SendWithUsException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SendWithUsMailService implements MailService {
    private static final Logger log = LoggerFactory.getLogger(SendWithUsMailService.class);

    private static final String KEY_ADDRESS = "address";
    private static final String KEY_NAME = "name";

    private SendWithUs client;

    @Autowired
    public SendWithUsMailService(SendWithUs client) {
        this.client = client;
    }

    @Override
    public void sendEmails(Collection<SendEmailRequest> requests) {
        for (SendEmailRequest request : requests) {
            sendEmail(request);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void sendEmail(SendEmailRequest request) {
        SendWithUsSendRequest swuRequest = new SendWithUsSendRequest();

        swuRequest.setEmailId(request.getTemplateId());

        if (StringUtils.isNotBlank(request.getRecipientEmail())) {
            if (StringUtils.isBlank(request.getRecipientName())) {
                swuRequest.setRecipient(ImmutableMap.of(KEY_ADDRESS, request.getRecipientEmail()));
            } else {
                swuRequest.setRecipient(ImmutableMap.of(KEY_NAME, request.getRecipientName(), KEY_ADDRESS, request.getRecipientEmail()));
            }
        }

        if (CollectionUtils.isNotEmpty(request.getBccEmails())) {
            List<Map<String, Object>> bccs =
                request.getBccEmails().stream()
                    .map(bcc -> {
                        Map<String, Object> bccMap = new HashMap<>();
                        bccMap.put(KEY_ADDRESS, bcc);
                        return bccMap;
                    })
                    .collect(Collectors.toList());

            Map<String, Object>[] bccArr = new HashMap[bccs.size()];
            bccArr = bccs.toArray(bccArr);

            swuRequest.setBccRecipients(bccArr);
        }

        swuRequest.setEmailData(request.getRequestData());

        try {
            client.send(swuRequest);
        } catch (SendWithUsException e) {
            log.error("{}", e);
            throw new RuntimeException(e);
        }
    }
}
