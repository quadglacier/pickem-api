package com.gci.pickem.config;

import com.sendwithus.SendWithUs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailConfig {

    @Value("${sendwithus.api-key}")
    private String apiKey;

    @Bean
    public SendWithUs emailClient() {
        return new SendWithUs(apiKey);
    }
}
