package com.gci.pickem.exception.notfound;

abstract class PickemMissingException extends RuntimeException {

    PickemMissingException(long id, PickemObjectType type) {
        super(String.format("No %s found with ID %d", type.getDescription(), id));
    }

    PickemMissingException(String message) {
        super(message);
    }
}
