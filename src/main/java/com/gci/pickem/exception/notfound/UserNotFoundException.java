package com.gci.pickem.exception.notfound;

public class UserNotFoundException extends PickemMissingException {

    public UserNotFoundException(Long userId) {
        super(userId, PickemObjectType.USER);
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
