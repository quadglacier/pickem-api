package com.gci.pickem.exception.notfound;

enum PickemObjectType {
    POOL("Pool"),
    USER("User");

    private final String description;

    PickemObjectType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
