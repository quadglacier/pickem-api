package com.gci.pickem.exception.notfound;

public class PoolNotFoundException extends PickemMissingException {

    public PoolNotFoundException(long id) {
        super(id, PickemObjectType.POOL);
    }
}
