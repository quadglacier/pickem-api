package com.gci.pickem.exception;

public class InvalidResponseReceivedException extends RuntimeException {

    public InvalidResponseReceivedException(String message) {
        super(message);
    }
}
