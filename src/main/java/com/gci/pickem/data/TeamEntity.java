package com.gci.pickem.data;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "teams")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "team_id", nullable = false)
    private Long teamId;

    @Column(name = "external_team_id", nullable = false)
    private Long externalId;

    @Column(name = "abbreviation", nullable = false)
    private String abbreviation;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "team_name", nullable = false)
    private String teamName;

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamEntity that = (TeamEntity) o;
        return Objects.equals(externalId, that.externalId) &&
                Objects.equals(abbreviation, that.abbreviation) &&
                Objects.equals(city, that.city) &&
                Objects.equals(teamName, that.teamName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(externalId, abbreviation, city, teamName);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
            .add("externalId", externalId)
            .add("abbreviation", abbreviation)
            .add("city", city)
            .add("teamName", teamName)
            .toString();
    }
}
