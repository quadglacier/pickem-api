package com.gci.pickem.data;

import javax.persistence.*;

@Entity
@Table(name = "pick_view_grants")
public class PickViewGrantEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pick_view_grant_id", nullable = false)
    private Long pickviewGrantId;

    @Column(name = "granting_user_id", nullable = false)
    private Long grantingUserId;

    @Column(name = "granted_pool_id", nullable = false)
    private Long grantedPoolId;

    @Column(name = "granted_user_id", nullable = false)
    private Long grantedUserId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "granting_user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserEntity grantingUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "granted_user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserEntity grantedUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "granted_pool_id", referencedColumnName = "pool_id", insertable = false, updatable = false)
    private PoolEntity grantedPool;

    public Long getPickviewGrantId() {
        return pickviewGrantId;
    }

    public void setPickviewGrantId(Long pickviewGrantId) {
        this.pickviewGrantId = pickviewGrantId;
    }

    public Long getGrantingUserId() {
        return grantingUserId;
    }

    public void setGrantingUserId(Long grantingUserId) {
        this.grantingUserId = grantingUserId;
    }

    public Long getGrantedPoolId() {
        return grantedPoolId;
    }

    public void setGrantedPoolId(Long grantedPoolId) {
        this.grantedPoolId = grantedPoolId;
    }

    public Long getGrantedUserId() {
        return grantedUserId;
    }

    public void setGrantedUserId(Long grantedUserId) {
        this.grantedUserId = grantedUserId;
    }

    public UserEntity getGrantingUser() {
        return grantingUser;
    }

    public void setGrantingUser(UserEntity grantingUser) {
        this.grantingUser = grantingUser;
    }

    public UserEntity getGrantedUser() {
        return grantedUser;
    }

    public void setGrantedUser(UserEntity grantedUser) {
        this.grantedUser = grantedUser;
    }

    public PoolEntity getGrantedPool() {
        return grantedPool;
    }

    public void setGrantedPool(PoolEntity grantedPool) {
        this.grantedPool = grantedPool;
    }
}
