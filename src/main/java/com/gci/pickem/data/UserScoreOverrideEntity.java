package com.gci.pickem.data;

import javax.persistence.*;

@Entity
@Table(name = "user_score_override")
public class UserScoreOverrideEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_score_override_id")
    private Long userScoreOverrideId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "pool_id")
    private Long poolId;

    @Column(name = "authorizing_user_id")
    private Long authorizingUserId;

    @Column(name = "season")
    private Integer season;

    @Column(name = "week")
    private Integer week;

    @Column(name = "score")
    private Integer score;

    public Long getUserScoreOverrideId() {
        return userScoreOverrideId;
    }

    public void setUserScoreOverrideId(Long userScoreOverrideId) {
        this.userScoreOverrideId = userScoreOverrideId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Long getAuthorizingUserId() {
        return authorizingUserId;
    }

    public void setAuthorizingUserId(Long authorizingUserId) {
        this.authorizingUserId = authorizingUserId;
    }

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
