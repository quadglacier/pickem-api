package com.gci.pickem.data;

import javax.persistence.*;

@Entity
@Table(name = "job_run_times")
public class JobRunTimeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_run_time_id", nullable = false)
    private Long jobRunTimeId;

    @Column(name = "schedule_updater")
    private Long scheduleUpdaterEpoch;

    @Column(name = "pick_notifier")
    private Long pickNotifierEpoch;

    public Long getJobRunTimeId() {
        return jobRunTimeId;
    }

    public void setJobRunTimeId(Long jobRunTimeId) {
        this.jobRunTimeId = jobRunTimeId;
    }

    public Long getScheduleUpdaterEpoch() {
        return scheduleUpdaterEpoch;
    }

    public void setScheduleUpdaterEpoch(Long scheduleUpdaterEpoch) {
        this.scheduleUpdaterEpoch = scheduleUpdaterEpoch;
    }

    public Long getPickNotifierEpoch() {
        return pickNotifierEpoch;
    }

    public void setPickNotifierEpoch(Long pickNotifierEpoch) {
        this.pickNotifierEpoch = pickNotifierEpoch;
    }
}
