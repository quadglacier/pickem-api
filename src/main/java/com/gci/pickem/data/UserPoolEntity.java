package com.gci.pickem.data;

import javax.persistence.*;

@Entity
@Table(name = "user_pool")
public class UserPoolEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_pool_id", nullable = false)
    private Long userPoolId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "pool_id", nullable = false)
    private Long poolId;

    @Column(name = "user_role", nullable = false)
    private String userRole;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserEntity user;

    @OneToOne
    @JoinColumn(name = "pool_id", referencedColumnName = "pool_id", insertable = false, updatable = false)
    private PoolEntity pool;

    public UserPoolEntity() {
    }

    public Long getUserPoolId() {
        return userPoolId;
    }

    public void setUserPoolId(Long userPoolId) {
        this.userPoolId = userPoolId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public PoolEntity getPool() {
        return pool;
    }

    public void setPool(PoolEntity pool) {
        this.pool = pool;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}
