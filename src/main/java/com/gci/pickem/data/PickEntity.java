package com.gci.pickem.data;

import javax.persistence.*;

@Entity
@Table(name = "picks")
public class PickEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pick_id", nullable = false)
    private Long pickId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "pool_id", nullable = false)
    private Long poolId;

    @Column(name = "game_id", nullable = false)
    private Long gameId;

    @Column(name = "chosen_team_id")
    private Long chosenTeamId;

    @Column(name = "confidence")
    private Integer confidence;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "game_id", referencedColumnName = "game_id", insertable = false, updatable = false)
    private GameEntity game;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserEntity user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pool_id", referencedColumnName = "pool_id", insertable = false, updatable = false)
    private PoolEntity pool;

    public PickEntity() {
    }

    public Long getPickId() {
        return pickId;
    }

    public void setPickId(Long pickId) {
        this.pickId = pickId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getChosenTeamId() {
        return chosenTeamId;
    }

    public void setChosenTeamId(Long chosenTeamId) {
        this.chosenTeamId = chosenTeamId;
    }

    public GameEntity getGame() {
        return game;
    }

    public void setGame(GameEntity game) {
        this.game = game;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public PoolEntity getPool() {
        return pool;
    }

    public void setPool(PoolEntity pool) {
        this.pool = pool;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }
}
