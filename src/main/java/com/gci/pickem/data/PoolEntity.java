package com.gci.pickem.data;

import com.gci.pickem.model.PoolView;

import javax.persistence.*;

@Entity
@Table(name = "pool")
public class PoolEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pool_id")
    private long poolId;

    @Column(name = "pool_name")
    private String poolName;

    @Column(name = "scoring_method")
    private Integer scoringMethod;

    @Column(name = "client_url")
    private String clientUrl;

    @Column(name = "pick_grace_period")
    private Long pickGracePeriod = 0L;

    public PoolEntity() {
    }

    public PoolEntity(PoolView poolView) {
        this.poolName = poolView.getName();
        this.scoringMethod = poolView.getScoringMethod();
        this.clientUrl = poolView.getClientUrl();
    }

    public long getPoolId() {
        return poolId;
    }

    public void setPoolId(long poolId) {
        this.poolId = poolId;
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public Integer getScoringMethod() {
        return scoringMethod;
    }

    public void setScoringMethod(Integer scoringMethod) {
        this.scoringMethod = scoringMethod;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }

    public Long getPickGracePeriod() {
        return pickGracePeriod;
    }

    public void setPickGracePeriod(Long pickGracePeriod) {
        this.pickGracePeriod = pickGracePeriod;
    }
}
