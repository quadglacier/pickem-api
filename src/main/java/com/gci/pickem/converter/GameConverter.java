package com.gci.pickem.converter;

import com.gci.pickem.data.GameEntity;
import com.gci.pickem.data.TeamEntity;
import com.gci.pickem.model.mysportsfeeds.v2.Game;
import com.gci.pickem.model.mysportsfeeds.v2.PlayedStatus;
import com.gci.pickem.model.mysportsfeeds.v2.Schedule;
import com.gci.pickem.service.team.TeamService;
import com.gci.pickem.util.ScheduleUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class GameConverter implements Converter<Game, GameEntity> {

    private TeamService teamService;

    @Autowired
    GameConverter(TeamService teamService) {
        this.teamService = teamService;
    }

    @Override
    public GameEntity convert(Game source) {
        GameEntity entity = new GameEntity();

        Schedule schedule = source.getSchedule();

        entity.setSeason(ScheduleUtil.getSeasonForDate(schedule.getStartTime().toInstant()));
        entity.setWeek(schedule.getWeek());
        entity.setExternalId(schedule.getId());

        entity.setGameTimeEpoch(TimeUnit.SECONDS.toMillis(schedule.getStartTime().toEpochSecond()));

        TeamEntity away = teamService.findByExternalId((long) schedule.getAwayTeam().getId());
        entity.setAwayTeamId(away.getTeamId());

        TeamEntity home = teamService.findByExternalId((long) schedule.getHomeTeam().getId());
        entity.setHomeTeamId(home.getTeamId());

        // Check if the entity has been finalized.
        if (PlayedStatus.COMPLETED == schedule.getPlayedStatus() && source.getScore() != null) {
            entity.setGameComplete(true);
            entity.setWinningTeamId(source.getScore().getHomeScoreTotal().compareTo(source.getScore().getAwayScoreTotal()) > 0 ? home.getTeamId() : away.getTeamId());
        } else {
            entity.setGameComplete(false);
        }

        entity.setAdminOverridden(false);

        return entity;
    }
}
