package com.gci.pickem.converter;

import com.gci.pickem.data.TeamEntity;
import com.gci.pickem.model.mysportsfeeds.BaseTeam;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TeamConverter implements Converter<BaseTeam, TeamEntity> {

    @Override
    public TeamEntity convert(BaseTeam source) {
        if (source == null) {
            return null;
        }

        TeamEntity entity = new TeamEntity();

        entity.setTeamName(source.getName());
        entity.setAbbreviation(source.getAbbreviation());
        entity.setCity(source.getCity());
        entity.setExternalId((long) source.getId());

        return entity;
    }
}
