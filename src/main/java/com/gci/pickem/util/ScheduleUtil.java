package com.gci.pickem.util;

import com.gci.pickem.exception.InvalidFormatException;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ScheduleUtil {

    private static final Pattern GAME_TIME_PATTERN = Pattern.compile("(1?[0-9]):([0-5][0-9])([AP]M)");

    private ScheduleUtil() {
    }

    public static int getSeasonForDate(Instant instant) {
        /*
         * The "season" corresponds to the earliest year games were played. If this game
         * took place late in the year, then the year we have is the season. If the game
         * took place in the beginning of the year, then we need to subtract one from the
         * year we have to get the season.
         */
        LocalDate localDate = instant.atZone(ZoneId.of(PickEmConstants.MSF_TIMEZONE)).toLocalDate();
        return localDate.getMonthValue() >= Month.AUGUST.getValue() ? localDate.getYear() : localDate.getYear() - 1;
    }

    static String get24HourTime(String time) {
        Matcher m = GAME_TIME_PATTERN.matcher(time);
        if (!m.matches()) {
            throw new InvalidFormatException(String.format("Unexpected game time format: %s", time));
        }

        String militaryTime = "";

        String hour = m.group(1);

        String timeOfDay = m.group(3);
        if ("PM".equals(timeOfDay) && !"12".equals(hour)) {
            militaryTime += Integer.toString(Integer.parseInt(hour) + 12);
        } else if ("AM".equals(timeOfDay) && "12".equals(hour)) {
            militaryTime += "00";
        } else {
            int intHour = Integer.parseInt(hour);
            if (intHour < 10) {
                militaryTime += "0";
            }

            militaryTime += hour;
        }

        militaryTime += ":" + m.group(2);

        return militaryTime;
    }
}
