package com.gci.pickem.repository;

import com.gci.pickem.data.GameEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GameRepository extends CrudRepository<GameEntity, Long> {

    List<GameEntity> findAllBySeasonAndWeek(Integer season, Integer week);

    @Query("SELECT MIN(g.week) FROM GameEntity g WHERE g.gameTimeEpoch > :epochTime")
    int getCurrentWeek(@Param("epochTime") long epochTime);

    GameEntity findByExternalId(Integer externalId);

    List<GameEntity> findByGameTimeEpochBetween(long start, long end);
}
