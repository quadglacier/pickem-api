package com.gci.pickem.repository;

import com.gci.pickem.data.PickViewGrantEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PickViewGrantRepository extends CrudRepository<PickViewGrantEntity, Long> {

    List<PickViewGrantEntity> findByGrantedUserId(Long grantedUserId);

    List<PickViewGrantEntity> findByGrantedUserIdAndGrantedPoolId(Long grantedUserId, Long grantedPoolId);
}
