package com.gci.pickem.repository;

import com.gci.pickem.data.JobRunTimeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JobRunTimeRepository extends CrudRepository<JobRunTimeEntity, Long> {

    List<JobRunTimeEntity> findAll();
}
