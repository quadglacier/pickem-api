package com.gci.pickem.repository;

import com.gci.pickem.data.PickEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface PickRepository extends CrudRepository<PickEntity, Long> {

    @Query(
        "SELECT p " +
        "FROM PickEntity p " +
        "JOIN FETCH p.game g " +
        "WHERE p.userId = :userId AND " +
        "   p.poolId = :poolId AND " +
        "   g.season = :season AND " +
        "   g.week = :week")
    Set<PickEntity> getPicks(@Param("userId") long userId,
                             @Param("poolId") long poolId,
                             @Param("season") int season,
                             @Param("week") int week);

    @Query(
        "SELECT p " +
        "FROM PickEntity p " +
        "JOIN FETCH p.game g " +
        "WHERE p.userId = :userId AND " +
        "   p.poolId = :poolId AND " +
        "   g.season = :season")
    Set<PickEntity> getPicks(@Param("userId") long userId,
                             @Param("poolId") long poolId,
                             @Param("season") int season);

    @Query(
        "SELECT p " +
        "FROM PickEntity p " +
        "JOIN FETCH p.game g " +
        "WHERE p.poolId = :poolId AND " +
        "   g.season = :season AND " +
        "   g.week = :week")
    Set<PickEntity> getPicks(@Param("poolId") long poolId,
                             @Param("season") int season,
                             @Param("week") int week);

    @Query(
        "SELECT p " +
        "FROM PickEntity p " +
        "JOIN FETCH p.game g " +
        "WHERE p.poolId = :poolId AND " +
        "   g.season = :season")
    Set<PickEntity> getPicks(@Param("poolId") long poolId,
                             @Param("season") int season);

    @Query(
        "SELECT COUNT(p) " +
        "FROM PickEntity p " +
        "JOIN p.game g " +
        "JOIN p.pool l " +
        "WHERE g.season = :season " +
        "   AND g.isGameComplete = true " +
        "   AND p.confidence = :confidence " +
        "   AND l.scoringMethod = 2")
    Integer getTotalPicksForConfidenceAndSeason(@Param("confidence") int confidence,
                                                @Param("season") int season);

    @Query(
        "SELECT COUNT(p) " +
        "FROM PickEntity p " +
        "JOIN p.game g " +
        "JOIN p.pool l " +
        "WHERE g.season = :season " +
        "   AND g.isGameComplete = true " +
        "   AND p.confidence = :confidence " +
        "   AND p.chosenTeamId = g.winningTeamId " +
        "   AND l.scoringMethod = 2")
    Integer getCorrectPicksForConfidenceAndSeason(@Param("confidence") int confidence,
                                                  @Param("season") int season);

    PickEntity getPickByUserIdAndPoolIdAndGameId(long userId, long poolId, long gameId);
}
