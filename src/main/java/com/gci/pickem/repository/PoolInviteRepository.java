package com.gci.pickem.repository;

import com.gci.pickem.data.PoolInviteEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;

public interface PoolInviteRepository extends CrudRepository<PoolInviteEntity, Long> {

    Collection<PoolInviteEntity> findByPoolId(long poolId);

    @Query(
        "SELECT pi " +
        "FROM PoolInviteEntity pi " +
        "JOIN FETCH pi.invitedUser u " +
        "WHERE u.userId = :userId AND pi.poolId = :poolId")
    Optional<PoolInviteEntity> findByUserIdAndPoolId(@Param("userId") long userId, @Param("poolId") long poolId);

    @Query(
        "SELECT pi " +
        "FROM PoolInviteEntity pi " +
        "JOIN FETCH pi.invitedUser u " +
        "WHERE u.userId = :userId")
    Collection<PoolInviteEntity> findByUserId(@Param("userId") long userId);
}
