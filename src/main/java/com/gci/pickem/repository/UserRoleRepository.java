package com.gci.pickem.repository;

import com.gci.pickem.data.UserRoleEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleRepository extends CrudRepository<UserRoleEntity, Long> {
}
