package com.gci.pickem.repository;

import com.gci.pickem.data.UserPoolEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface UserPoolRepository extends CrudRepository<UserPoolEntity, Long> {

    Set<UserPoolEntity> findByUserId(Long userId);

    Set<UserPoolEntity> findByPoolId(Long poolId);
}
