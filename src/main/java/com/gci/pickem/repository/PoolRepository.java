package com.gci.pickem.repository;

import com.gci.pickem.data.PoolEntity;
import org.springframework.data.repository.CrudRepository;

public interface PoolRepository extends CrudRepository<PoolEntity, Long> {
}
