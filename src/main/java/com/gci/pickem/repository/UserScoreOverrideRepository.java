package com.gci.pickem.repository;

import com.gci.pickem.data.UserScoreOverrideEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface UserScoreOverrideRepository extends CrudRepository<UserScoreOverrideEntity, Long> {

    Set<UserScoreOverrideEntity> findByUserIdAndPoolIdAndSeason(Long userId, Long poolId, Integer season);

    UserScoreOverrideEntity findByUserIdAndPoolIdAndSeasonAndWeek(Long userId, Long poolId, Integer season, Integer week);
}
