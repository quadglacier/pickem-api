package com.gci.pickem.repository;

import com.gci.pickem.data.TeamEntity;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<TeamEntity, Long> {

    TeamEntity findByExternalId(Long externalId);
}
