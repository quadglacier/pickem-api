package com.gci.pickem.job;

import com.gci.pickem.service.job.JobRunTimeService;
import com.gci.pickem.service.schedule.ScheduleService;
import com.gci.pickem.util.PickEmConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Component
public class ScheduleUpdater extends AbstractJob {
    private static final Logger log = LoggerFactory.getLogger(ScheduleUpdater.class);

    private ScheduleService scheduleService;

    @Autowired
    ScheduleUpdater(ScheduleService scheduleService, JobRunTimeService jobRunTimeService) {
        super(jobRunTimeService);
        this.scheduleService = scheduleService;
    }

    // Run just after midnight of every day, eastern time.
//    @Scheduled(cron = "0 5 0 * * *", zone = PickEmConstants.MSF_TIMEZONE)
    @Scheduled(cron = "0 1/5 * * * *", zone = PickEmConstants.MSF_TIMEZONE)
    public void updateFutureSchedules() {
        // We only want this method to run once per day. However, since Heroku is currently putting
        // the application to sleep after some time with no activity, it's possible (probable, in fact)
        // that the app won't be running at midnight every day. As a result, we want to make sure that
        // once the app is up and running, it tries to do this as long as it's been over a day since
        // the last run. This will result in the process slowly creeping further and further from being
        // run at midnight, but since we're looking out seven days, it likely will never matter that much.
        Long lastRunEpoch = jobRunTimeService.getLastScheduleUpdaterRunTime();
        if (lastRunEpoch == null || Instant.now().toEpochMilli() - MILLIS_PER_DAY > lastRunEpoch) {
            log.info("Executing schedule updates process.");

            // Look for all games in the next 7 days.
            scheduleService.processExternalGamesForNextDays(7);

            // Set the run time to the most recent midnight. Try to keep future runs as close to midnight as possible.
            jobRunTimeService.updateLastScheduleUpdaterRunTime(getTodayMidnightEpoch());

            log.info("Schedule process update complete.");
        } else {
            log.info("Hasn't yet been a day since the last schedule updater run.");
        }
    }

    // Check for updated scores every 5 minutes of every day.
    @Scheduled(cron = "0 0/5 * * * *", zone = PickEmConstants.MSF_TIMEZONE)
    public void updateScores() {
        log.info("Checking for updated scores");

        Instant today = Instant.now();
        Instant yesterday = today.minus(1, ChronoUnit.DAYS);

        // Look back over the last day and find any scores for games that are finished.
        scheduleService.processScoresForDate(yesterday);
        scheduleService.processScoresForDate(today);

        log.info("Done checking for updated scores");
    }
}
