package com.gci.pickem.job;

import com.gci.pickem.service.job.JobRunTimeService;
import com.gci.pickem.service.picks.PickService;
import com.gci.pickem.util.PickEmConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class MissingPickNotifier extends AbstractJob {
    private static final Logger log = LoggerFactory.getLogger(MissingPickNotifier.class);

    private PickService pickService;

    @Autowired
    MissingPickNotifier(PickService pickService, JobRunTimeService jobRunTimeService) {
        super(jobRunTimeService);
        this.pickService = pickService;
    }

    // Run at 2am Eastern time every day, after schedule updates have been processed.
//    @Scheduled(cron = "0 0 2 * * *", zone = PickEmConstants.MSF_TIMEZONE)
    @Scheduled(cron = "0 2/5 * * * *", zone = PickEmConstants.MSF_TIMEZONE)
    public void notifyUsers() {
        // We only want this method to run once per day. However, since Heroku is currently putting
        // the application to sleep after some time with no activity, it's possible (probable, in fact)
        // that the app won't be running at midnight every day. As a result, we want to make sure that
        // once the app is up and running, it tries to do this as long as it's been over a day since
        // the last run. This will result in the process slowly creeping further and further from being
        // run at midnight, but since we're looking out seven days, it likely will never matter that much.
        Long lastRunEpoch = jobRunTimeService.getLastPickNotifierRunTime();
        if (lastRunEpoch == null || Instant.now().toEpochMilli() - MILLIS_PER_DAY > lastRunEpoch) {
            log.info("Executing notifyUsers process.");

            // Don't pass a date, use today.
            pickService.notifyUsersWithoutPicks(null);

            jobRunTimeService.updateLastPickNotifierRunTime(getTodayMidnightEpoch());

            log.info("notifyUsers process complete.");
        } else {
            log.info("Hasn't yet been a day since the last pick notifier run.");
        }
    }
}
