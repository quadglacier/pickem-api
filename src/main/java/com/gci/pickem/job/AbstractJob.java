package com.gci.pickem.job;

import com.gci.pickem.service.job.JobRunTimeService;
import com.gci.pickem.util.PickEmConstants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public abstract class AbstractJob {

    protected JobRunTimeService jobRunTimeService;

    protected static final long MILLIS_PER_DAY = TimeUnit.DAYS.toMillis(1);

    protected AbstractJob(JobRunTimeService jobRunTimeService) {
        this.jobRunTimeService = jobRunTimeService;
    }

    protected long getTodayMidnightEpoch() {
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(ZoneId.of(PickEmConstants.MSF_TIMEZONE));
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);

        return todayMidnight.atZone(ZoneId.of(PickEmConstants.MSF_TIMEZONE)).toInstant().toEpochMilli();
    }
}
