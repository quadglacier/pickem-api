package com.gci.pickem.model;

import java.util.List;

public class UserPoolListResponse {

    private String firstName;
    private String lastName;
    private List<UserPoolView> poolViews;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<UserPoolView> getPoolViews() {
        return poolViews;
    }

    public void setPoolViews(List<UserPoolView> poolViews) {
        this.poolViews = poolViews;
    }
}
