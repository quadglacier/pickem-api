package com.gci.pickem.model;

import java.util.List;

public class ScoreResponse {

    private long poolId;
    private int season;

    // Week is optional, sometimes it's not used.
    private Integer week;

    private List<UserScoreView> userScores;

    public long getPoolId() {
        return poolId;
    }

    public void setPoolId(long poolId) {
        this.poolId = poolId;
    }

    public int getSeason() {
        return season;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public List<UserScoreView> getUserScores() {
        return userScores;
    }

    public void setUserScores(List<UserScoreView> userScores) {
        this.userScores = userScores;
    }
}
