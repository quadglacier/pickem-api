package com.gci.pickem.model;

public class UserScoreView extends UserView {

    private int weekScore;
    private int weekPossiblePointsRemaining;
    private int ytdScore;

    public int getWeekScore() {
        return weekScore;
    }

    public void setWeekScore(int weekScore) {
        this.weekScore = weekScore;
    }

    public int getWeekPossiblePointsRemaining() {
        return weekPossiblePointsRemaining;
    }

    public void setWeekPossiblePointsRemaining(int weekPossiblePointsRemaining) {
        this.weekPossiblePointsRemaining = weekPossiblePointsRemaining;
    }

    public int getYtdScore() {
        return ytdScore;
    }

    public void setYtdScore(int ytdScore) {
        this.ytdScore = ytdScore;
    }
}
