package com.gci.pickem.model;

import com.gci.pickem.data.PoolEntity;

// Consider things like max size, etc in the future.
public class PoolView {

    private String name;
    private Integer scoringMethod;
    private String clientUrl;

    public PoolView() {
    }

    public PoolView(PoolEntity pool) {
        this.name = pool.getPoolName();
        this.scoringMethod = pool.getScoringMethod();
        this.clientUrl = pool.getClientUrl();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScoringMethod() {
        return scoringMethod;
    }

    public void setScoringMethod(Integer scoringMethod) {
        this.scoringMethod = scoringMethod;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }
}
