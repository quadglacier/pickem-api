package com.gci.pickem.model;

import java.math.BigDecimal;

public class PickData {

    private String message;

    private String homeTeam;
    private BigDecimal homePickPct;
    private BigDecimal homeAvgConfidence;

    private String awayTeam;
    private BigDecimal awayPickPct;
    private BigDecimal awayAvgConfidence;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public BigDecimal getHomePickPct() {
        return homePickPct;
    }

    public void setHomePickPct(BigDecimal homePickPct) {
        this.homePickPct = homePickPct;
    }

    public BigDecimal getHomeAvgConfidence() {
        return homeAvgConfidence;
    }

    public void setHomeAvgConfidence(BigDecimal homeAvgConfidence) {
        this.homeAvgConfidence = homeAvgConfidence;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public BigDecimal getAwayPickPct() {
        return awayPickPct;
    }

    public void setAwayPickPct(BigDecimal awayPickPct) {
        this.awayPickPct = awayPickPct;
    }

    public BigDecimal getAwayAvgConfidence() {
        return awayAvgConfidence;
    }

    public void setAwayAvgConfidence(BigDecimal awayAvgConfidence) {
        this.awayAvgConfidence = awayAvgConfidence;
    }
}
