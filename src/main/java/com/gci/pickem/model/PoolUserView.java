package com.gci.pickem.model;

public class PoolUserView {

    private String firstName;
    private String lastName;
    private String poolRole;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPoolRole() {
        return poolRole;
    }

    public void setPoolRole(String poolRole) {
        this.poolRole = poolRole;
    }
}
