package com.gci.pickem.model.mysportsfeeds;

public interface BaseTeam {

    Integer getId();
    String getCity();
    String getAbbreviation();
    String getName();
}
