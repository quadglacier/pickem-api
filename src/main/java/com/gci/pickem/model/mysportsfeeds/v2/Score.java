package com.gci.pickem.model.mysportsfeeds.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Score {

    private Integer awayScoreTotal;
    private Integer homeScoreTotal;

    public Integer getAwayScoreTotal() {
        return awayScoreTotal;
    }

    public void setAwayScoreTotal(Integer awayScoreTotal) {
        this.awayScoreTotal = awayScoreTotal;
    }

    public Integer getHomeScoreTotal() {
        return homeScoreTotal;
    }

    public void setHomeScoreTotal(Integer homeScoreTotal) {
        this.homeScoreTotal = homeScoreTotal;
    }
}
