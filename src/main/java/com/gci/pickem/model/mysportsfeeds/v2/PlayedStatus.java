package com.gci.pickem.model.mysportsfeeds.v2;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PlayedStatus {
    @JsonProperty("UNPLAYED")
    UNPLAYED,
    @JsonProperty("LIVE")
    LIVE,
    @JsonProperty("COMPLETED")
    COMPLETED,
    @JsonProperty("COMPLETED_PENDING_REVIEW")
    COMPLETED_PENDING_REVIEW;
}
