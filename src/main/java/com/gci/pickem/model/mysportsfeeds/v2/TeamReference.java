package com.gci.pickem.model.mysportsfeeds.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gci.pickem.model.mysportsfeeds.BaseTeam;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamReference implements BaseTeam {

    private Integer id;
    private String city;
    private String name;
    private String abbreviation;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
}
