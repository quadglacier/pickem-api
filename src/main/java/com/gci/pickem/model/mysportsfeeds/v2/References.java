package com.gci.pickem.model.mysportsfeeds.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class References {

    private List<TeamReference> teamReferences;

    public List<TeamReference> getTeamReferences() {
        return teamReferences;
    }

    public void setTeamReferences(List<TeamReference> teamReferences) {
        this.teamReferences = teamReferences;
    }
}
