package com.gci.pickem.model.mysportsfeeds.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Game {

    private Schedule schedule;
    private Score score;

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(schedule, game.schedule) &&
                Objects.equals(score, game.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(schedule, score);
    }
}
