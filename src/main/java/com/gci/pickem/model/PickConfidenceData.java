package com.gci.pickem.model;

import java.math.BigDecimal;

public class PickConfidenceData {

    private int totalPicks;
    private int correctPicks;
    private BigDecimal accuracy;

    public int getTotalPicks() {
        return totalPicks;
    }

    public void setTotalPicks(int totalPicks) {
        this.totalPicks = totalPicks;
    }

    public int getCorrectPicks() {
        return correctPicks;
    }

    public void setCorrectPicks(int correctPicks) {
        this.correctPicks = correctPicks;
    }

    public BigDecimal getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(BigDecimal accuracy) {
        this.accuracy = accuracy;
    }
}
