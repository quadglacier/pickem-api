package com.gci.pickem.model;

import java.util.Map;

public class PickDataResponse {

    private Integer season;
    private Integer week;
    private Long poolId;
    private Map<Long, PickData> dataByGame;

    public Integer getSeason() {
        return season;
    }

    public void setSeason(Integer season) {
        this.season = season;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Map<Long, PickData> getDataByGame() {
        return dataByGame;
    }

    public void setDataByGame(Map<Long, PickData> dataByGame) {
        this.dataByGame = dataByGame;
    }
}
