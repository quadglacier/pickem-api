package com.gci.pickem.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public final class Game {

    private long gameId;
    private TeamView homeTeam;
    private TeamView awayTeam;
    private boolean gameComplete;
    private TeamView winningTeam;

    @Deprecated
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime gameTime;

    private Long gameTimeEpoch;

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public TeamView getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(TeamView homeTeam) {
        this.homeTeam = homeTeam;
    }

    public TeamView getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(TeamView awayTeam) {
        this.awayTeam = awayTeam;
    }

    public LocalDateTime getGameTime() {
        return gameTime;
    }

    public void setGameTime(LocalDateTime gameTime) {
        this.gameTime = gameTime;
    }

    public Long getGameTimeEpoch() {
        return gameTimeEpoch;
    }

    public void setGameTimeEpoch(Long gameTimeEpoch) {
        this.gameTimeEpoch = gameTimeEpoch;
    }

    public boolean isGameComplete() {
        return gameComplete;
    }

    public void setGameComplete(boolean gameComplete) {
        this.gameComplete = gameComplete;
    }

    public TeamView getWinningTeam() {
        return winningTeam;
    }

    public void setWinningTeam(TeamView winningTeam) {
        this.winningTeam = winningTeam;
    }
}
