package com.gci.pickem.model;

public class UserPoolView {

    private String poolName;
    private String userRole;
    private Long poolId;
    private Long pickGracePeriod;

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Long getPickGracePeriod() {
        return pickGracePeriod;
    }

    public void setPickGracePeriod(Long pickGracePeriod) {
        this.pickGracePeriod = pickGracePeriod;
    }
}
