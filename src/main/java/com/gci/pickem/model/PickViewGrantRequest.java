package com.gci.pickem.model;

import java.util.Collection;

public class PickViewGrantRequest {

    private long poolId;
    private Collection<Long> grantedUserIds;

    public long getPoolId() {
        return poolId;
    }

    public void setPoolId(long poolId) {
        this.poolId = poolId;
    }

    public Collection<Long> getGrantedUserIds() {
        return grantedUserIds;
    }

    public void setGrantedUserIds(Collection<Long> grantedUserIds) {
        this.grantedUserIds = grantedUserIds;
    }
}
