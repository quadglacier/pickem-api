package com.gci.pickem.model;

import java.util.List;

public class PoolUserListResponse {

    private String poolName;
    private List<PoolUserView> users;

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public List<PoolUserView> getUsers() {
        return users;
    }

    public void setUsers(List<PoolUserView> users) {
        this.users = users;
    }
}
