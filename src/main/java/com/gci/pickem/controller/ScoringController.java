package com.gci.pickem.controller;

import com.gci.pickem.exception.MissingRequiredDataException;
import com.gci.pickem.model.ScoreResponse;
import com.gci.pickem.model.UserScoreOverrideRequest;
import com.gci.pickem.model.UserView;
import com.gci.pickem.service.schedule.ScheduleService;
import com.gci.pickem.service.scoring.ScoringService;
import com.gci.pickem.util.RequestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Map;

@RestController
public class ScoringController {
    private static final Logger log = LoggerFactory.getLogger(ScoringController.class);

    private ScoringService scoringService;
    private ScheduleService scheduleService;

    @Autowired
    ScoringController(
        ScoringService scoringService,
        ScheduleService scheduleService
    ) {
        this.scoringService = scoringService;
        this.scheduleService = scheduleService;
    }

    @GetMapping("/api/v1/score")
    @PreAuthorize("hasAuthority('USER')")
    public ScoreResponse getScore(@RequestParam("season") Integer season,
                                  @RequestParam("week") Integer week,
                                  @RequestParam("poolId") Long poolId,
                                  HttpServletRequest request) {
        UserView user = RequestUtil.getRequestUser(request);
        return scoringService.getScore(user.getId(), poolId, season, week);
    }

    @GetMapping("/api/v1/score/week")
    @PreAuthorize("hasAuthority('USER')")
    public ScoreResponse getScoreForWeek(@RequestParam("season") Integer season,
                                         @RequestParam("week") Integer week,
                                         @RequestParam("poolId") Long poolId,
                                         HttpServletRequest request) {
        UserView user = RequestUtil.getRequestUser(request);
        return scoringService.getWeekScores(user.getId(), poolId, season, week);
    }

    @GetMapping("/api/v1/score/season")
    @PreAuthorize("hasAuthority('USER')")
    public ScoreResponse getScoreForSeason(@RequestParam("season") Integer season,
                                           @RequestParam("poolId") Long poolId,
                                           HttpServletRequest request) {
        UserView user = RequestUtil.getRequestUser(request);
        return scoringService.getSeasonScores(user.getId(), poolId, season);
    }

    @PostMapping("/api/v1/score/process")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void processGameScoresForDate(@RequestBody Map<String, Long> input) {
        Long epoch = input.get("epoch");
        if (epoch == null) {
            throw new MissingRequiredDataException("Missing required parameter 'epoch'.");
        }

        log.info("Processing scores for epoch {}", epoch);

        scheduleService.processScoresForDate(Instant.ofEpochMilli(epoch));
    }

    @PostMapping("/api/v1/score/override")
    @PreAuthorize("hasAuthority('USER')")
    public void overrideUserScore(@RequestBody UserScoreOverrideRequest overrideRequest, HttpServletRequest request) {
        UserView userView = RequestUtil.getRequestUser(request);

        scoringService.overrideUserScore(
            userView.getId(),
            overrideRequest.getUserId(),
            overrideRequest.getPoolId(),
            overrideRequest.getSeason(),
            overrideRequest.getWeek(),
            overrideRequest.getScore());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleException(RuntimeException e, HttpServletResponse response) throws IOException {
        log.error("Exception: {}", e.getMessage());
        response.getOutputStream().write(e.getMessage().getBytes());
    }
}
